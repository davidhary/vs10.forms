' General Information about an assembly is controlled through the following 
Imports System.Reflection

' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

<Assembly: AssemblyTitle("ISR Windows Forms VB Library")> 
<Assembly: AssemblyDescription("ISR Windows Forms VB Library")> 
<Assembly: AssemblyCompany("Integrated Scientific Resources, Inc.")> 
<Assembly: AssemblyProduct("ISR Windows Forms VB Library 1.2")> 
<Assembly: AssemblyCopyright("(c) 2009 Scientific Resources, Inc. All rights reserved.")> 
<Assembly: AssemblyTrademark("isr.WindowsForms and ISR logo are trademarks of Integrated Scientific Resources, Inc.")> 
<Assembly: CLSCompliant(True)> 
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 
<Assembly: AssemblyCulture("")> 

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: System.Runtime.InteropServices.ComVisible(False)> 

' The following GUID is for the ID of the type library if this project is exposed to COM
' <Assembly: Guid("2DC81684-153C-4E08-87A5-2487DB7E45DE")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:
<Assembly: AssemblyVersion("1.2.*")> 

' The file version specifies the version that is deployed.
' By commenting this out we assume that these versions are the same.
<Assembly: AssemblyFileVersion("1.2.0.0")> 
<Assembly: AssemblyInformationalVersion("1.2.0.0")> 

