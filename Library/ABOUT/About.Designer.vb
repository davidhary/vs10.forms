<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
 Partial Class About
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me._iconPictureBox = New System.Windows.Forms.PictureBox
        Me._productTitleLabel = New System.Windows.Forms.Label
        Me._descriptionLabel = New System.Windows.Forms.Label
        Me._licenseLabel = New System.Windows.Forms.Label
        Me._copyrightLabel = New System.Windows.Forms.Label
        Me._exitButton = New System.Windows.Forms.Button
        Me._productNameLabel = New System.Windows.Forms.Label
        CType(Me._iconPictureBox, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_iconPictureBox
        '
        Me._iconPictureBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._iconPictureBox.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._iconPictureBox.Location = New System.Drawing.Point(315, 4)
        Me._iconPictureBox.Name = "_iconPictureBox"
        Me._iconPictureBox.Size = New System.Drawing.Size(16, 16)
        Me._iconPictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize
        Me._iconPictureBox.TabIndex = 8
        Me._iconPictureBox.TabStop = False
        '
        '_productTitleLabel
        '
        Me._productTitleLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._productTitleLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._productTitleLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._productTitleLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._productTitleLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._productTitleLabel.Location = New System.Drawing.Point(3, 0)
        Me._productTitleLabel.Name = "_productTitleLabel"
        Me._productTitleLabel.Size = New System.Drawing.Size(292, 18)
        Me._productTitleLabel.TabIndex = 14
        Me._productTitleLabel.Text = "Product Title"
        Me._productTitleLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_descriptionLabel
        '
        Me._descriptionLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._descriptionLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._descriptionLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._descriptionLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._descriptionLabel.Location = New System.Drawing.Point(3, 58)
        Me._descriptionLabel.Name = "_descriptionLabel"
        Me._descriptionLabel.Size = New System.Drawing.Size(332, 36)
        Me._descriptionLabel.TabIndex = 10
        Me._descriptionLabel.Text = "File description, copyrights, trademarks, revision"
        '
        '_licenseLabel
        '
        Me._licenseLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._licenseLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._licenseLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._licenseLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._licenseLabel.Location = New System.Drawing.Point(3, 138)
        Me._licenseLabel.Name = "_licenseLabel"
        Me._licenseLabel.Size = New System.Drawing.Size(332, 33)
        Me._licenseLabel.TabIndex = 12
        Me._licenseLabel.Text = "This product is licensed to"
        '
        '_copyrightLabel
        '
        Me._copyrightLabel.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._copyrightLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._copyrightLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._copyrightLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._copyrightLabel.Location = New System.Drawing.Point(3, 178)
        Me._copyrightLabel.Name = "_copyrightLabel"
        Me._copyrightLabel.Size = New System.Drawing.Size(332, 29)
        Me._copyrightLabel.TabIndex = 11
        Me._copyrightLabel.Text = "This program is protected by US and international copyright laws"
        Me._copyrightLabel.TextAlign = System.Drawing.ContentAlignment.TopCenter
        '
        '_exitButton
        '
        Me._exitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._exitButton.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._exitButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._exitButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._exitButton.Location = New System.Drawing.Point(287, 94)
        Me._exitButton.Name = "_exitButton"
        Me._exitButton.Size = New System.Drawing.Size(32, 32)
        Me._exitButton.TabIndex = 13
        Me._exitButton.UseVisualStyleBackColor = False
        '
        '_productNameLabel
        '
        Me._productNameLabel.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me._productNameLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._productNameLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._productNameLabel.ForeColor = System.Drawing.Color.FromArgb(CType(CType(0, Byte), Integer), CType(CType(0, Byte), Integer), CType(CType(148, Byte), Integer))
        Me._productNameLabel.Location = New System.Drawing.Point(3, 31)
        Me._productNameLabel.Name = "_productNameLabel"
        Me._productNameLabel.Size = New System.Drawing.Size(330, 18)
        Me._productNameLabel.TabIndex = 9
        Me._productNameLabel.Text = "Product Name"
        Me._productNameLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        'AboutNew
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.Color.FromArgb(CType(CType(232, Byte), Integer), CType(CType(243, Byte), Integer), CType(CType(255, Byte), Integer))
        Me.CancelButton = Me._exitButton
        Me.ClientSize = New System.Drawing.Size(338, 216)
        Me.Controls.Add(Me._iconPictureBox)
        Me.Controls.Add(Me._productTitleLabel)
        Me.Controls.Add(Me._descriptionLabel)
        Me.Controls.Add(Me._licenseLabel)
        Me.Controls.Add(Me._copyrightLabel)
        Me.Controls.Add(Me._exitButton)
        Me.Controls.Add(Me._productNameLabel)
        Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedToolWindow
        Me.Name = "AboutNew"
        Me.Text = "About"
        CType(Me._iconPictureBox, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _iconPictureBox As System.Windows.Forms.PictureBox
    Private WithEvents _productTitleLabel As System.Windows.Forms.Label
    Private WithEvents _descriptionLabel As System.Windows.Forms.Label
    Private WithEvents _licenseLabel As System.Windows.Forms.Label
    Private WithEvents _copyrightLabel As System.Windows.Forms.Label
    Private WithEvents _exitButton As System.Windows.Forms.Button
    Private WithEvents _productNameLabel As System.Windows.Forms.Label
End Class
