Imports System.Security.Permissions
Imports isr.Core.AssemblyInfoExtensions
''' <summary>Displays assembly information.</summary>
''' <remarks>This is the information form for assemblies.
'''   To open, instantiate the form passing the new instance the module file version 
'''   information reference.</remarks>
''' <license>
''' (c) 2002 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/10/04" by="David Hary" revision="1.0.1501.x">
''' Include in isr.Support.dll
''' </history>
''' <history date="09/21/02" by="David Hary" revision="1.0.839.x">
''' created.
''' </history>
Public Class About

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' onInstantiate()

    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary>
    ''' Adds a drop shaddow parameter.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>
    ''' From Code Project: http://www.codeproject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)> _
        Get
            Dim cp As Windows.Forms.CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " METHODS "

    ''' <summary>Displays this module.</summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong">
    '''   Failed to show main form</exception>
    ''' <param name="executionAssembly">Specifies the 
    '''   <see cref="System.Reflection.Assembly">calling assembly</see>.</param>
    ''' <param name="licenseeName">A <see cref="System.String">String</see> expression that specifies
    '''     the name of the licensee</param>
    ''' <param name="licenseCode">A <see cref="System.String">String</see> expression that specifies
    '''     the license string (serial number)</param>
    ''' <param name="systemId">A <see cref="System.String">String</see> expression that specifies
    '''     the system id, e.g., the Product Name.</param>
    ''' <param name="licenseHeader">A <see cref="System.String">String</see> expression that specifies the license
    '''   heading under which the licensee name and code are displayed.  For
    '''   example:  "This product is licensed to:".</param>
    ''' <example>This example displays the About form.
    '''   <code>
    '''     Private Sub displayAboutForm()
    '''       ' display the application information
    '''       Dim aboutScreen As New isr.WindowsForms.About
    '''       aboutScreen.ShowDialog(System.Reflection.Assembly.GetExecutingAssembly, _
    '''       String.Empty, String.Empty, String.Empty, String.Empty)
    '''     End Sub
    '''   </code>
    '''   To run this example, paste the code fragment into the method region
    '''   of a Visual Basic form.  Run the program by pressing F5.
    ''' </example>
    ''' <remarks>Use this method to display the application main form. You must 
    '''   also call Application.Run with reference to this form because the method
    '''   shows the form modeless.</remarks>
    <FileIOPermission(SecurityAction.LinkDemand, Unrestricted:=True)> _
  Public Overloads Sub Show( _
    ByVal executionAssembly As System.Reflection.Assembly, _
    ByVal licenseeName As String, _
    ByVal licenseCode As String, _
    ByVal systemId As String, _
    ByVal licenseHeader As String)

        If executionAssembly Is Nothing Then
            Throw New ArgumentNullException("executionAssembly")
        End If
        If licenseeName Is Nothing Then
            Throw New ArgumentNullException("licenseeName")
        End If
        If licenseCode Is Nothing Then
            Throw New ArgumentNullException("licenseCode")
        End If
        If systemId Is Nothing Then
            Throw New ArgumentNullException("systemId")
        End If
        If licenseHeader Is Nothing Then
            Throw New ArgumentNullException("licenseHeader")
        End If

        ' process the form show methods
        Me.beforeFormLoad(executionAssembly, licenseeName, licenseCode, _
            systemId, licenseHeader, "Product Information")

        ' show the form
        MyBase.Show()

    End Sub

    ''' <summary>Displays this module.</summary>
    ''' <exception cref="ArgumentException" guarantee="strong">
    '''   Failed to show main form</exception>
    ''' <returns>A Dialog Result</returns>
    ''' <param name="executionAssembly">Specifies the 
    '''   <see cref="System.Reflection.Assembly">calling assembly</see>.</param>
    ''' <param name="licenseeName">is an optional String expression that specifies
    '''     the name of the licensee</param>
    ''' <param name="licenseCode">is an optional String expression that specifies
    '''     the license string (serial number)</param>
    ''' <param name="systemId">is an optional String expression that specifies
    '''     the system id, e.g., the Product Name.</param>
    ''' <param name="licenseHeader">A <see cref="System.String">String</see> expression that specifies the license
    '''   heading under which the licensee name and code are displayed.  For
    '''   example:  "This product is licensed to:".</param>
    ''' <example>This example displays the About form.
    '''   <code>
    '''     Private Sub displayAboutForm()
    '''       ' display the application information
    '''       Dim aboutScreen As New isr.WindowsForms.About
    '''       aboutScreen.ShowDialog(support.GetFileVersionInfo(), _
    '''       String.Empty, String.Empty, String.Empty, String.Empty)
    '''     End Sub
    '''   </code>
    '''   To run this example, paste the code fragment into the method region
    '''   of a Visual Basic form.  Run the program by pressing F5.
    ''' </example>
    ''' <remarks>Use this method to display the form. If you prefer to use the Show Form 
    '''   statement, make sure that all the errors trapped in the Form_Load event 
    '''   are handled within the form as these errors cannot be raised to the 
    '''   calling function.</remarks>
    <System.Security.Permissions.FileIOPermission(SecurityAction.LinkDemand, Unrestricted:=True)> _
  Public Overloads Function ShowDialog( _
    ByVal executionAssembly As System.Reflection.Assembly, _
    ByVal licenseeName As String, _
    ByVal licenseCode As String, _
    ByVal systemId As String, _
    ByVal licenseHeader As String) As System.Windows.Forms.DialogResult

        If executionAssembly Is Nothing Then
            Throw New ArgumentNullException("executionAssembly")
        End If
        If licenseeName Is Nothing Then
            Throw New ArgumentNullException("licenseeName")
        End If
        If licenseCode Is Nothing Then
            Throw New ArgumentNullException("licenseCode")
        End If
        If systemId Is Nothing Then
            Throw New ArgumentNullException("systemId")
        End If
        If licenseHeader Is Nothing Then
            Throw New ArgumentNullException("licenseHeader")
        End If

        ' process the form show methods
        Me.beforeFormLoad(executionAssembly, licenseeName, licenseCode, _
            systemId, licenseHeader, "Product Information")

        ' show the form
        Return MyBase.ShowDialog()

    End Function

#End Region

#Region " PROPERTIES "

    Private _statusMessage As String = String.Empty
    ''' <summary>Gets or sets the status message.</summary>
    ''' <value>A <see cref="System.String">String</see>.</value>
    ''' <remarks>Use this property to get the status message generated by the object.</remarks>
    Public ReadOnly Property StatusMessage() As String
        Get
            Return _statusMessage
        End Get
    End Property

    Public Property Image() As System.Drawing.Icon
        Get
            Return Me.Icon
        End Get
        Set(ByVal value As System.Drawing.Icon)
            If value IsNot Nothing Then
                With Me._iconPictureBox
                    .Image = value.ToBitmap
                    .Invalidate()
                    If .Left + .Width > Me.Width Then
                        If .Width < 0.2 * Me.Width Then
                            .Left = Me.ClientSize.Width - .Width - 5
                        End If
                    End If
                End With
            End If
        End Set
    End Property

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Occurs when the user selects the exit button.</summary>
    ''' <remarks>Use this method to exit.</remarks>
    Private Sub exitButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles _exitButton.Click

        ' Close this form
        Me.Close()

    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Initializes the user interface and tool tips.</summary>
    ''' <remarks>Call this method from the form load method to set the user interface.</remarks>
    Private Sub initializeUserInterface()
        '  _toolTip.SetToolTip(Me.txtDuration, "Enter count-down duration in seconds")
        '  _toolTip.SetToolTip(Me._exitButton, "Click to exit")
        '  _toolTip.SetToolTip(Me.aboutButton, String.Format(System.Globalization.CultureInfo.CurrentCulture, _
        '     "this is{0}a three line{0}tool tip", Environment.NewLine))

        Me._iconPictureBox.Image = Me.Icon.ToBitmap

    End Sub

    ''' <summary>Processes the display of this module.</summary>
    ''' <exception cref="ArgumentNullException" guarantee="strong">
    '''   Failed to show main form</exception>
    ''' <param name="executionAssembly">Specifies the 
    '''   <see cref="System.Reflection.Assembly">calling assembly</see>.</param>
    ''' <param name="licenseeName">A <see cref="System.String">String</see> expression that specifies
    '''     the name of the licensee</param>
    ''' <param name="licenseCode">A <see cref="System.String">String</see> expression that specifies
    '''     the license string (serial number)</param>
    ''' <param name="systemId">A <see cref="System.String">String</see> expression that specifies
    '''     the system id, e.g., the Product Name.</param>
    ''' <param name="licenseHeader">A <see cref="System.String">String</see> expression that specifies the license
    '''   heading under which the licensee name and code are displayed.  For
    '''   example:  "This product is licensed to:".</param>
    ''' <history date="07/24/04" by="David Hary" revision="1.0.1666.x">
    '''   Remove width settings and use anchor as measurement does not work
    ''' </history>
    ''' <history date="02/09/08" by="David Hary" revision="1.0.2961.x">
    '''  Use File Desction (Application Title Property) for the product name and
    '''  remove it from the comments section.  Use product version instead of file version.
    ''' </history>
    <System.Security.Permissions.FileIOPermission(SecurityAction.LinkDemand, Unrestricted:=True)> _
  Private Sub beforeFormLoad( _
      ByVal executionAssembly As System.Reflection.Assembly, _
      ByVal licenseeName As String, _
      ByVal licenseCode As String, _
      ByVal systemId As String, _
      ByVal licenseHeader As String, _
      ByVal caption As String)

        If executionAssembly Is Nothing Then
            Throw New ArgumentNullException("executionAssembly")
        End If
        If licenseeName Is Nothing Then
            Throw New ArgumentNullException("licenseeName")
        End If
        If licenseCode Is Nothing Then
            Throw New ArgumentNullException("licenseCode")
        End If
        If systemId Is Nothing Then
            Throw New ArgumentNullException("systemId")
        End If
        If licenseHeader Is Nothing Then
            Throw New ArgumentNullException("licenseHeader")
        End If

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' hide the cancel button
            Me._exitButton.Top = -Me._exitButton.Height

            Dim fileVersionInfo As System.Diagnostics.FileVersionInfo = _
                System.Diagnostics.FileVersionInfo.GetVersionInfo(executionAssembly.Location)

            Dim rowTop As Int32
            Dim rowText As System.Text.StringBuilder = New System.Text.StringBuilder("")
            Dim topMargin As Int32 = 1
            ' Dim leftMargin As Int32 = 2
            Dim rowSpace As Int32

            ' set the caption
            If caption.Length > 0 Then
                Me.Text = caption
            Else
                Me.Text = "Registration Information"
            End If

            With _productTitleLabel
                rowSpace = .Height \ 2
                .Text = executionAssembly.GetName.Name()
                .Refresh()
                .Top = topMargin
                '.Left = leftMargin
                '.Width = 1 + Int32.parse(Me.CreateGraphics().MeasureString(_productTitleLabel.text, .Font).Width, System.Globalization.CultureInfo.CurrentCulture)
                .Height = Convert.ToInt32(Me.CreateGraphics().MeasureString(_productTitleLabel.Text, .Font).Height, System.Globalization.CultureInfo.CurrentCulture)
                .Invalidate()
                rowTop = .Top + .Height + rowSpace
            End With

            With _productNameLabel
                .Text = fileVersionInfo.FileDescription
                .Refresh()
                '.Width = 1 + Int32.parse(.CreateGraphics().MeasureString(_productNameLabel.text, _productNameLabel.Font).Width, System.Globalization.CultureInfo.CurrentCulture)
                .Height = Convert.ToInt32(.CreateGraphics().MeasureString(_productNameLabel.Text, _productNameLabel.Font).Height, System.Globalization.CultureInfo.CurrentCulture)
                '.Left = leftMargin
                .Top = rowTop
                .Invalidate()
                rowTop = .Top + .Height + rowSpace
            End With

            With _iconPictureBox
                .Top = topMargin
            End With

            If fileVersionInfo.Comments.Trim.Length > 0 Then
                rowText.Append(fileVersionInfo.Comments.Trim)
            End If
            If systemId.Trim.Length > 0 Then
                rowText.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{1}", Environment.NewLine, systemId.Trim)
            End If
            rowText.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}Version: {1} updated {2}", Environment.NewLine, _
                fileVersionInfo.ProductVersion, _
                System.IO.File.GetLastWriteTime(System.Windows.Forms.Application.StartupPath))

            With _descriptionLabel
                .Text = rowText.ToString
                .Refresh()
                '.Width = 1 + Int32.parse(Me.CreateGraphics().MeasureString(_descriptionLabel.text, _descriptionLabel.Font).Width, System.Globalization.CultureInfo.CurrentCulture)
                .Height = Convert.ToInt32(Me.CreateGraphics().MeasureString(_descriptionLabel.Text, _descriptionLabel.Font).Height, System.Globalization.CultureInfo.CurrentCulture)
                '.Left = leftMargin
                .Top = rowTop
                .Invalidate()
                rowTop = .Top + .Height + rowSpace
            End With

            ' Display license information
            rowText = New System.Text.StringBuilder("")
            If (licenseeName.Trim.Length > 0) Or _
               (licenseCode.Trim.Length > 0) Then
                If licenseHeader.Trim.Length > 0 Then
                    rowText.Append(licenseHeader)
                End If
                If licenseeName.Trim.Length > 0 Then
                    If rowText.Length > 0 Then
                        rowText.Append(Environment.NewLine)
                    End If
                    rowText.AppendFormat(Globalization.CultureInfo.CurrentCulture, " {0}", licenseeName.Trim)
                End If
                If licenseCode.Trim.Length > 0 Then
                    If rowText.Length > 0 Then
                        rowText.Append(Environment.NewLine)
                    End If
                    rowText.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {0}", licenseCode.Trim)
                End If
            ElseIf licenseHeader.Trim.Length > 0 Then
                rowText.Append(licenseHeader)
            End If
            With _licenseLabel
                .Text = rowText.ToString
                .Refresh()
                '.Width = 1 + Int32.parse(Me.CreateGraphics().MeasureString(_licenseLabel.text, _licenseLabel.Font).Width, System.Globalization.CultureInfo.CurrentCulture)
                .Height = Convert.ToInt32(Me.CreateGraphics().MeasureString(_licenseLabel.Text, _licenseLabel.Font).Height, System.Globalization.CultureInfo.CurrentCulture)
                '.Left = leftMargin
                .Top = rowTop
                .Invalidate()
                rowTop = .Top + .Height + rowSpace
            End With

            With _copyrightLabel
                .Text = fileVersionInfo.LegalCopyright.Trim
                .Refresh()
                '.Width = Me.ClientSize.Width - leftMargin - leftMargin
                '.Left = leftMargin
                .Top = rowTop
                .Invalidate()
                rowTop = .Top + .Height + rowSpace \ 2
            End With

            ' set form height to fit data.
            Me.Height = rowTop + (Me.Height - Me.ClientSize.Height)

        Catch

            Throw

        Finally

            ' Turn off the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs after the form is closed.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>This event is a notification that the form has already gone away before
    ''' control is returned to the calling method (in case of a modal form).  Use this
    ''' method to delete any temporary files that were created or dispose of any objects
    ''' not disposed with the closing event.
    ''' </remarks>
    Private Sub form_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles MyBase.Closed

    End Sub

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to allow canceling the closing of the form.
    '''   Because the form is not yet closed at this point, this is also the best 
    '''   place to serialize a form's visible properties, such as size and 
    '''   location.</remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) _
        Handles MyBase.Closing

        ' disable the timer if any
        ' actionTimer.Enabled = False
        System.Windows.Forms.Application.DoEvents()

        ' set module objects that reference other objects to Nothing

        ' terminate form objects
        'Me.terminateObjects()

    End Sub

    ''' <summary>
    ''' Occurs when the form is loaded.
    ''' Does all the pre-processing before the form controls are rendered as the user expects them.
    ''' </summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' center the form
            Me.CenterToScreen()

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            ' Turn off the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

    Private Sub form_Activated(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Activated
    End Sub

    ''' <summary>
    ''' Does all the post processing after all the form controls are rendered as the user expects them.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    <CodeAnalysis.SuppressMessage("Microsoft.Mobility", "CA1601:DoNotUseTimersThatPreventPowerStateChanges")> _
    Private Sub form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        ' allow form rendering time to complete: process all messages currently in the queue.
        Windows.Forms.Application.DoEvents()

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            ' Me.instantiateObjects()

            ' set the form caption
            Me.Text = My.Application.Info.ExtendedCaption()

            ' set tool tips
            initializeUserInterface()

            ' allow some events to occur for refreshing the display.
            Windows.Forms.Application.DoEvents()

        Catch ex As Exception

            Windows.Forms.MessageBox.Show(ex.ToString, "Exception Occurred", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Exclamation, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

End Class

