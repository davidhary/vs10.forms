﻿Imports System.Windows.Forms
''' <summary>
''' Includes extensions for forms.
''' </summary>
''' <remarks></remarks>
''' <license>
''' (c) 2010 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/17/2010" by="David Hary" revision="1.2.3669.x">
''' created.
''' </history>
Public Module FormExtensions

  ''' <summary>
  ''' Test form value setter.
  ''' </summary>
  ''' <param name="form"></param>
  ''' <remarks><example>
  ''' <code>
  ''' form.SafeThreadAction(AddressOf textSetter)
  ''' </code></example></remarks>
  Public Sub Setter(ByVal form As Form)
    form.Text = "Written by the background thread"
  End Sub

  ''' <summary>
  ''' Test form value setter.
  ''' </summary>
  ''' <param name="form"></param>
  ''' <remarks><example>
  ''' <code>
  ''' form.SafeThreadSetter(AddressOf textSetter, value)
  ''' </code></example></remarks>
  Public Sub Setter(ByVal form As Form, ByVal value As String)
    form.Text = value
  End Sub

  ''' <summary>
  ''' Tests form value getter.
  ''' </summary>
  ''' <param name="form"></param>
  ''' <returns></returns>
  ''' <remarks><example><code>
  ''' form.SafeThreadGetter(AddressOf Getter)
  ''' </code></example></remarks>
  Public Function Getter(ByVal form As Form) As System.String
    Dim result As String = form.Text
    Return result
  End Function

  ''' <summary>
  ''' Safely run an action from a thread.
  ''' </summary>
  ''' <typeparam name="T">Specifies a Form type.</typeparam>
  ''' <param name="form">Specifies reference to a form.</param>
  ''' <param name="action">Spoecifies reference to a function implementing the action</param>
  ''' <remarks>
  ''' <example>
  ''' <code>
  ''' </code>
  ''' </example>
  ''' </remarks>
  <System.Runtime.CompilerServices.Extension()> _
  Public Sub SafeThreadAction(Of T As Form)(ByVal form As T, ByVal action As Action(Of T))
    If form.InvokeRequired Then
      form.BeginInvoke(action, form)
    Else
      action(form)
    End If
  End Sub

  ''' <summary>
  ''' Safely run a setter action from a thread.
  ''' </summary>
  ''' <typeparam name="T">Specifies a Form type.</typeparam>
  ''' <typeparam name="TType">Specifies the set type.</typeparam>
  ''' <param name="form">Specifies reference to a form.</param>
  ''' <param name="action">Spoecifies reference to a function implementing the action</param>
  ''' <param name="value">Specifies a value to set.</param>
  ''' <remarks>
  ''' <example>
  ''' <code>
  ''' </code>
  ''' </example>
  ''' </remarks>
  <System.Runtime.CompilerServices.Extension()> _
  Public Sub SafeThreadSetter(Of T As Form, TType)(ByVal form As T, ByVal action As Action(Of T, TType), ByVal value As TType)
    If form.InvokeRequired Then
      form.BeginInvoke(action, form, value)
    Else
      action(form, value)
    End If
  End Sub

  ''' <summary>
  ''' Safely gets a value from a thread.
  ''' </summary>
  ''' <typeparam name="T">Specifies a form type.</typeparam>
  ''' <typeparam name="TType">A type which to return</typeparam>
  ''' <param name="form">Specifes reference to a form.</param>
  ''' <param name="getter">Reference to a getter function</param>
  ''' <returns></returns>
  ''' <remarks>
  ''' <example>
  ''' <code>
  ''' </code>
  ''' </example>
  ''' </remarks>
  <System.Runtime.CompilerServices.Extension()> _
  Public Function SafeThreadGetter(Of T As Form, TType)(ByVal form As T, ByVal getter As Func(Of T, TType)) As TType
    If form.InvokeRequired Then
      Dim result As IAsyncResult = form.BeginInvoke(getter, form)
      Dim result2 As Object = form.EndInvoke(result)
      Return CType(result2, TType)
    Else
      Return getter(form)
    End If
  End Function

End Module
