Imports System.Drawing

''' <summary>Provides share support for Windows Controls.</summary>
''' <remarks>Use this class methods and properties to support Windows Controls 
'''   functionality..</remarks>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="01/03/03" by="David Hary" revision="1.0.1105.x">
''' created.
''' </history>
Public NotInheritable Class Helper

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>Prevents instantiation of the class.</summary>
    Private Sub New()
    End Sub

#End Region

#Region " SHARED "

    ''' <summary>Returns the control requesting help based on the mouse
    '''   position clicked.</summary>
    ''' <param name="containerControl">Specifiesthe container control requesting the help.</param>
    ''' <returns>Returns the control upon which the operator clicked with
    '''   the help icon or nothing if no control was clicked.</returns>
    ''' <remarks></remarks>
    Public Shared Function ControlRequestingHelp(ByVal containerControl As Windows.Forms.Control, _
    ByVal helpEvent As Windows.Forms.HelpEventArgs) _
      As Windows.Forms.Control

        If containerControl Is Nothing Then
            Return Nothing
        End If

        If helpEvent Is Nothing Then
            Return Nothing
        End If

        ' Convert screen coordinates to client coordinates
        Dim clientCoordinatePoint As System.Drawing.Point = containerControl.PointToClient(helpEvent.MousePos)

        ' look for the control upon which the operator clicked.
        For i As Int32 = 0 To containerControl.Controls.Count
            If containerControl.Controls.Item(i).Bounds.Contains(clientCoordinatePoint) Then
                Return containerControl.Controls.Item(i)
            End If
        Next

        ' if no control was located, return nothing
        Return Nothing

    End Function

    ''' <summary>Builds a column style for a data grid.</summary>
    ''' <param name="headerText"></param>
    ''' <param name="mappingName"></param>
    ''' <param name="width"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <history></history>
    Public Shared Function BuildColumnStyle(ByVal headerText As String, _
      ByVal mappingName As String, ByVal width As Int32) As Windows.Forms.DataGridColumnStyle

        Dim cs As New Windows.Forms.DataGridTextBoxColumn
        cs.HeaderText = headerText
        cs.MappingName = mappingName
        cs.Width = width
        Return cs

    End Function

    ''' <summary>Searches the combo box and selects a located item.</summary>
    ''' <param name="combo">Specifies an instance of a ComboBox.</param>
    ''' <param name="e">Specifies an instance of the 
    '''   <see cref="System.Windows.Forms.KeyPressEventArgs">event arguments</see>,
    '''   which specify the key pressed.</param>
    ''' <remarks>Use this method to search and select combo box index.</remarks>
    Public Shared Function SearchAndSelect( _
      ByVal combo As Windows.Forms.ComboBox, _
      ByVal e As System.Windows.Forms.KeyPressEventArgs) As Int32

        If combo Is Nothing Then
            Return -1
        End If

        If e IsNot Nothing AndAlso Char.IsControl(e.KeyChar) Then
            Return combo.SelectedIndex
        Else
            Dim cursorPosition As Int32 = combo.SelectionStart
            Dim selectionLength As Int32 = combo.SelectionLength
            Dim itemNumber As Int32 = combo.FindString(combo.Text)
            If itemNumber >= 0 Then
                ' if we have a match, select the current item and reposition the cursor
                combo.SelectedIndex = itemNumber
                combo.SelectionStart = cursorPosition
                combo.SelectionLength = selectionLength
                Return itemNumber
            Else
                Return combo.SelectedIndex
            End If
        End If

    End Function

    ''' <summary>Searches the combo box and selects a located item
    '''   upon releasing a key.</summary>
    ''' <param name="combo">Specifies an instance of a ComboBox.</param>
    ''' <param name="e">Specifies an instance of the 
    '''   <see cref="System.Windows.Forms.KeyEventArgs">event arguments</see>,
    '''   which specify the key released.</param>
    ''' <remarks>Use this method to search and select combo box index.</remarks>
    Public Shared Function SearchAndSelect( _
        ByVal combo As Windows.Forms.ComboBox, _
        ByVal e As System.Windows.Forms.KeyEventArgs) As Int32

        If combo Is Nothing Then
            Throw New ArgumentNullException("combo")
        End If
        If e Is Nothing Then
            Throw New ArgumentNullException("e")
        End If

        If e.KeyCode.ToString.Length > 1 Then
            ' if a control character, skip
            Return combo.SelectedIndex
        Else
            Dim cursorPosition As Int32 = combo.SelectionStart
            Dim selectionLength As Int32 = combo.SelectionLength
            Dim itemNumber As Int32 = combo.FindString(combo.Text)
            If itemNumber >= 0 Then
                ' if we have a match, select the current item and reposition the cursor
                combo.SelectedIndex = itemNumber
                combo.SelectionStart = cursorPosition
                combo.SelectionLength = selectionLength
                Return itemNumber
            Else
                Return combo.SelectedIndex
            End If
        End If

    End Function

    ''' <summary>Copies properties from a source to a destination controls.</summary>
    ''' <param name="source">
    '''   specifies an instance of the control which
    '''   properties are copied from.</param>
    ''' <param name="destination">
    '''   specifies an instance of the control which
    '''   properties are copied to.</param>
    ''' <remarks>Use this method to copy the properties of a control to a new control.</remarks>
    ''' <example>This example adds a button to a form.
    '''   <code>
    '''     Private buttons As New ArrayList()
    '''     Private Sub addButton(ByVal sourceButton As Button)
    '''       Dim newButton As New Button()
    '''       WinFormsSupport.CopyControlProperties(sourceButton, newButton)
    '''       Me.Controls.Add(newButton)
    '''       AddHandler newButton.Click, AddressOf exitButton_Click
    '''       With newButton
    '''         .Name = String.Format( System.Globalization.CultureInfo.CurrentCulture, "newButton {0}", buttons.count.toString)
    '''         .Text = .Name
    '''         .Top = .Top + .Height * (buttons.Count + 1)
    '''         buttons.Add(newButton)
    '''       End With
    '''     End Sub
    '''   </code>
    '''   To run this example, paste the code fragment into the method region
    '''   of a Visual Basic form.  Run the program by pressing F5.
    ''' </example>
    Public Shared Sub CopyControlProperties( _
      ByVal source As System.Windows.Forms.Control, _
      ByVal destination As System.Windows.Forms.Control)

        Dim dontCopyNames() As String = {"WindowTarget"}
        Dim pinfos(), pinfo As System.Reflection.PropertyInfo
        If Not (source Is Nothing OrElse destination Is Nothing) Then
            pinfos = source.GetType.GetProperties()
            For Each pinfo In pinfos
                ' copy only those properties without parameters - you'll rarely need indexed properties
                If pinfo.GetIndexParameters().Length = 0 Then
                    ' skip properties that appear in the list of disallowed names.
                    If Array.IndexOf(dontCopyNames, pinfo.Name) < 0 Then
                        ' can only copy those properties that can be read and written.  
                        If pinfo.CanRead And pinfo.CanWrite Then
                            ' set the destination based on the source.
                            pinfo.SetValue(destination, pinfo.GetValue(source, Nothing), Nothing)
                        End If
                    End If
                End If
            Next
        End If

    End Sub

    ''' <summary>validates all controls in the container control.</summary>
    ''' <param name="validatedContainerControl">specifies the 
    '''   instance of the container control which controls are to be validated.</param>
    ''' <returns>Returns false if any control failed to validated.</returns>
    ''' <remarks></remarks>
    Public Shared Function ValidatedControls(ByVal validatedContainerControl As Windows.Forms.ContainerControl) As Boolean
        If validatedContainerControl Is Nothing Then
            Return False
        End If
        Dim controls As Windows.Forms.Control() = Helper.RetrieveControls(validatedContainerControl.Controls)
        For i As Int32 = 0 To controls.Length - 1
            ' validate the selected control
            controls(i).Focus()
            If Not validatedContainerControl.Validate() Then
                Return False
            End If
        Next
        Return True
    End Function

    ''' <summary>Retrieve all controls and child controls.</summary>
    ''' <param name="winFormControls"></param>
    ''' <returns>Returns an array of controls</returns>
    ''' <remarks>Make sure to send contrll back at lowest depth first so that most
    '''   child controls are checked for things before container controls, e.g., 
    '''   a TextBox is checked before a GroupBox control.</remarks>
    ''' <history date="04/15/05" by="David Hary" revision="1.0.1566.x">
    '''   Bug fix.  Return empty controls rather than nothing if no controls.
    ''' </history>
    Public Shared Function RetrieveControls(ByVal winFormControls As Windows.Forms.Control.ControlCollection) _
    As Windows.Forms.Control()

        Dim controlList As New ArrayList
        ' Dim allControls As ArrayList = New ArrayList
        Dim myQueue As Queue = New Queue
        ' add controls to the queue
        myQueue.Enqueue(winFormControls)

        Dim myControls As Windows.Forms.Control.ControlCollection
        'Dim current As Object
        Do While myQueue.Count > 0
            ' remove and return the object at the begining of the queue
            ' current = myQueue.Dequeue()
            If TypeOf myQueue.Peek Is Windows.Forms.Control.ControlCollection Then
                myControls = CType(myQueue.Dequeue, Windows.Forms.Control.ControlCollection)
                If myControls.Count > 0 Then
                    For i As Int32 = 0 To myControls.Count - 1
                        Dim myControl As Windows.Forms.Control = myControls.Item(i)
                        ' add this control to the array list
                        controlList.Add(myControl)
                        ' check if this control has a collection
                        If myControl.Controls IsNot Nothing Then
                            ' if so, add to the queue
                            myQueue.Enqueue(myControl.Controls)
                        End If
                    Next
                    'For Each myControl As Windows.Forms.Control In myControls
                    ' add this control to the array list
                    'controlList.Add(myControl)
                    ' check if this control has a collection
                    'If Not IsNothing(myControl.Controls) Then
                    ' if so, add to the queue
                    ' myQueue.Enqueue(myControl.Controls)
                    'End If
                    'Next
                End If
            Else
                myQueue.Dequeue()
            End If
        Loop
        If controlList.Count > 0 Then
            Dim allControls(controlList.Count - 1) As Windows.Forms.Control
            controlList.CopyTo(allControls)
            Return allControls
        Else
            Return New Windows.Forms.Control() {}
        End If

    End Function

#End Region

#Region " CONTROL IMAGES "

    ''' <summary>
    ''' Get image rectange withing the specified <para>Control</para>.
    ''' </summary>
    ''' <param name="currentImage"></param>
    ''' <param name="control"></param>
    ''' <param name="imageAlign"></param>
    ''' <param name="fillHeight"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function GetImageRect(ByVal currentImage As Image, ByVal control As Windows.Forms.Control, _
      ByVal imageAlign As ContentAlignment, ByVal fillHeight As Boolean) As Rectangle

        If currentImage Is Nothing OrElse control Is Nothing Then
            Return Rectangle.Empty
        End If

        Dim x As Integer = 0, y As Integer = 0, w As Integer = currentImage.Width + 1, h As Integer = currentImage.Height + 1

        If w > control.Width Then
            w = control.Width
        End If

        If h > control.Width Then
            h = control.Width
        End If

        Select Case imageAlign
            Case ContentAlignment.TopLeft
                x = 0
                y = 0

                Exit Select
            Case ContentAlignment.TopCenter
                x = (control.Width - w) \ 2
                y = 0

                If (control.Width - w) Mod 2 > 0 Then
                    x += 1
                End If

                Exit Select
            Case ContentAlignment.TopRight
                x = control.Width - w
                y = 0

                Exit Select
            Case ContentAlignment.MiddleLeft
                x = 0
                y = (control.Height - h) \ 2

                If (control.Height - h) Mod 2 > 0 Then
                    y += 1
                End If

                Exit Select
            Case ContentAlignment.MiddleCenter
                x = (control.Width - w) \ 2
                y = (control.Height - h) \ 2

                If (control.Width - w) Mod 2 > 0 Then
                    x += 1
                End If
                If (control.Height - h) Mod 2 > 0 Then
                    y += 1
                End If

                Exit Select
            Case ContentAlignment.MiddleRight
                x = control.Width - w
                y = (control.Height - h) \ 2

                If (control.Height - h) Mod 2 > 0 Then
                    y += 1
                End If

                Exit Select
            Case ContentAlignment.BottomLeft
                x = 0
                y = control.Height - h

                If (control.Height - h) Mod 2 > 0 Then
                    y += 1
                End If

                Exit Select
            Case ContentAlignment.BottomCenter
                x = (control.Width - w) \ 2
                y = control.Height - h

                If (control.Width - w) Mod 2 > 0 Then
                    x += 1
                End If

                Exit Select
            Case ContentAlignment.BottomRight
                x = control.Width - w
                y = control.Height - h

                Exit Select
        End Select

        If fillHeight AndAlso h < control.Height Then
            h = control.Height
        End If

        If x > 0 Then
            x -= 1
        End If
        If y > 0 Then
            y -= 1
        End If

        Return New Rectangle(x, y, w, h)
    End Function

    ''' <summary>
    ''' Returns an image or nothing.
    ''' </summary>
    ''' <param name="imageList"></param>
    ''' <param name="imageName"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function SelectImage(ByVal imageList As Windows.Forms.ImageList, ByVal imageName As String) As Image
        If String.IsNullOrEmpty(imageName) OrElse imageList Is Nothing OrElse Not imageList.Images.ContainsKey(imageName) Then
            Return Nothing
        End If
        Return imageList.Images(imageName)
    End Function

#End Region

End Class
