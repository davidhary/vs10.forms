''' <summary>Displays a splash screen.</summary>
''' <license>
''' (c) 2004 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="02/17/04" by="David Hary" revision="1.0.1508.x">
''' created.
''' </history>
Public Class SplashScreen

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary>
    ''' Adds a drop shaddow parameter.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>
    ''' From Code Project: http://www.codeproject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)> _
        Get
            Dim cp As Windows.Forms.CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " PROPERTIES and METRHODS "

    Dim _topmost As Boolean
    ''' <summary>
    ''' Sets the top most status in a thread safe way.
    ''' </summary>
    ''' <param name="value"></param>
    ''' <remarks></remarks>
    Public Sub TopmostSetter(ByVal value As Boolean)
        _topmost = value
        _infoType = InfoLabel.TopMost
        startSafeFormAccessThread()
    End Sub

    Private _licenseHeader As String = "Licensed to:"
    ''' <summary>Gets or sets the header for displaying the licensee name</summary>
    ''' <value><c>LicenseHeader</c>is a String property</value>
    Public Property LicenseHeader() As String
        Get
            Return _licenseHeader
        End Get
        Set(ByVal value As String)
            _licenseHeader = value
        End Set
    End Property

    Private _licenseCode As String = String.Empty
    ''' <summary>Gets or sets the license code</summary>
    ''' <value><c>LicenseCode</c>is a String property</value>
    Public Property LicenseCode() As String
        Get
            Return _licenseCode
        End Get
        Set(ByVal value As String)
            _licenseCode = value
        End Set
    End Property

    Private _licenseeName As String = "Integrated Scientific Resources, Inc."
    ''' <summary>Gets or sets the licensee name</summary>
    ''' <value><c>LicenseeName</c>is a String property</value>
    Public Property LicenseeName() As String
        Get
            Return _licenseeName
        End Get
        Set(ByVal value As String)
            _licenseeName = value
            _infoType = InfoLabel.License
            startSafeFormAccessThread()
        End Set
    End Property

    Private _platformTitle As String = String.Empty
    ''' <summary>Gets or sets the platform title, e.g., 'For 32 Bit operating Systems'</summary>
    ''' <value><c>PlatformTitle</c>is a String property</value>
    Public Property PlatformTitle() As String
        Get
            If String.IsNullOrEmpty(_platformTitle) Then
                _platformTitle = "for 32-Bit Operating Systems"
            End If
            Return _platformTitle
        End Get
        Set(ByVal value As String)
            _platformTitle = value
        End Set
    End Property

    ''' <summary>Gets or sets reference to the primary Picture Box</summary>
    Public ReadOnly Property PrimaryPictureBox() As Windows.Forms.PictureBox
        Get
            Return Me._companyLogoPictureBox
        End Get
    End Property

    ''' <summary>Gets or sets reference to the secondary Picture Box</summary>
    Public ReadOnly Property SecondaryPictureBox() As Windows.Forms.PictureBox
        Get
            Return Me._disksPictureBox
        End Get
    End Property

    Private _productFamilyName As String = "Member of ISR Software Components"
    ''' <summary>Gets or sets the product family name, such as MyCompany
    '''   Components</summary>
    ''' <value><c>ProductFamilyName</c>is a String property</value>
    Public Property ProductFamilyName() As String
        Get
            Return _productFamilyName
        End Get
        Set(ByVal value As String)
            _productFamilyName = value
        End Set
    End Property

    ''' <summary>
    ''' Updates all information.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub UpdateAllInfo()
        _infoType = InfoLabel.Status
        startSafeFormAccessThread()
    End Sub

    Private _status As String
    ''' <summary>
    ''' Holds the splash status.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Status() As String
        Get
            Return Me._statusLabel.Text
        End Get
        Set(ByVal value As String)
            _infoType = InfoLabel.Status
            _status = value
            startSafeFormAccessThread()
        End Set
    End Property

    ''' <summary>
    ''' Update the information on screen.
    ''' </summary>
    Private Sub updateInfo()

        Select Case _infoType
            Case InfoLabel.None
            Case InfoLabel.All
                _productFamilyLabel.Text = Me.ProductFamilyName
                _productFamilyLabel.Invalidate()
                _productTitleLabel.Text = My.Application.Info.Title
                _productTitleLabel.Invalidate()
                _productNameLabel.Text = My.Application.Info.ProductName
                _productNameLabel.Invalidate()
                _productVersionLabel.Text = String.Format(Globalization.CultureInfo.CurrentCulture, "Version {0}", My.Application.Info.Version)
                _productVersionLabel.Invalidate()
                _platformTitleLabel.Text = Me.PlatformTitle
                _platformTitleLabel.Invalidate()
                _companyNameLabel.Text = My.Application.Info.CompanyName
                _companyNameLabel.Invalidate()
                _copyrightLabel.Text = My.Application.Info.Copyright
                _copyrightLabel.Invalidate()
                _copyrightWarningLabel.Text = My.Application.Info.Trademark
                _copyrightWarningLabel.Invalidate()

                ' Display license information
                Dim licenseBuilder As System.Text.StringBuilder = New System.Text.StringBuilder("")
                If (Me.LicenseeName.Trim.Length > 0) Or _
                   (Me.LicenseCode.Trim.Length > 0) Then
                    If LicenseHeader.Trim.Length > 0 Then
                        licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{1}{0}", Environment.NewLine, LicenseHeader)
                    End If
                    If LicenseeName.Trim.Length > 0 Then
                        licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {1}{0}", Environment.NewLine, LicenseeName.Trim)
                    End If
                    If LicenseCode.Trim.Length > 0 Then
                        licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {1}{0}", Environment.NewLine, LicenseCode.Trim)
                    End If
                ElseIf LicenseHeader.Trim.Length > 0 Then
                    licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{1}{0}", Environment.NewLine, LicenseHeader)
                End If
                _licenseeLabel.Text = licenseBuilder.ToString
                _licenseeLabel.Height = Convert.ToInt32(Me.CreateGraphics().MeasureString(_licenseeLabel.Text, _licenseeLabel.Font).Height)
                _licenseeLabel.Invalidate()
                Me._statusLabel.Text = _status
                Me._statusLabel.Invalidate()
                MyBase.TopMost = _topmost
            Case InfoLabel.License
                ' Display license information
                Dim licenseBuilder As System.Text.StringBuilder = New System.Text.StringBuilder("")
                If (Me.LicenseeName.Trim.Length > 0) Or _
                   (Me.LicenseCode.Trim.Length > 0) Then
                    If LicenseHeader.Trim.Length > 0 Then
                        licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{1}{0}", Environment.NewLine, LicenseHeader)
                    End If
                    If LicenseeName.Trim.Length > 0 Then
                        licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {1}{0}", Environment.NewLine, LicenseeName.Trim)
                    End If
                    If LicenseCode.Trim.Length > 0 Then
                        licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "  {1}{0}", Environment.NewLine, LicenseCode.Trim)
                    End If
                ElseIf LicenseHeader.Trim.Length > 0 Then
                    licenseBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{1}{0}", Environment.NewLine, LicenseHeader)
                End If
                _licenseeLabel.Text = licenseBuilder.ToString
                _licenseeLabel.Height = Convert.ToInt32(Me.CreateGraphics().MeasureString(_licenseeLabel.Text, _licenseeLabel.Font).Height)
                _licenseeLabel.Invalidate()
            Case InfoLabel.Status
                Me._statusLabel.Text = _status
                Me._statusLabel.Invalidate()
            Case InfoLabel.TopMost
                MyBase.TopMost = _topmost
        End Select
    End Sub

#End Region

#Region " TYPES "

    ''' <summary>
    ''' Enumrates which labels to update using the thread safe update method.
    ''' </summary>
    ''' <remarks></remarks>
    Private Enum InfoLabel
        None
        All
        Status
        License
        TopMost
    End Enum

#End Region

#Region " THREAD SAFE DISPLAY "

    ''' <summary>
    ''' Specifies which information gets updated when calling the form 
    ''' access method from a thread safe delegate.
    ''' </summary>
    ''' <remarks></remarks>
    Private _infoType As InfoLabel

    ''' <summary>
    ''' Start the information updating thread.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub startSafeFormAccessThread()
        If Me.InvokeRequired Then
            Me.BeginInvoke(New Action(AddressOf updateInfo))
        Else
            Me.updateInfo()
        End If
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>
    ''' Does all the post processing after all the form controls are rendered as the user expects them.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes")> _
    Private Sub form_Shown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shown

        Windows.Forms.Application.DoEvents()

        Try

            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            Me._infoType = InfoLabel.All
            Me.updateInfo()

        Catch ex As Exception

            Windows.Forms.MessageBox.Show(ex.ToString, "Exception Occurred", Windows.Forms.MessageBoxButtons.OK, Windows.Forms.MessageBoxIcon.Exclamation, Windows.Forms.MessageBoxDefaultButton.Button1, Windows.Forms.MessageBoxOptions.DefaultDesktopOnly)

        Finally

            Me.Cursor = System.Windows.Forms.Cursors.Default
            Windows.Forms.Application.DoEvents()

        End Try

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>Hides the splash screen.</summary>
    Private Sub callOffButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
        Handles _cancelButton.Click
        Me.Close()
    End Sub

#End Region

End Class
