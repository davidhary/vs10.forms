<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ExceptionDisplay
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
     Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me._mainTabControl = New System.Windows.Forms.TabControl
        Me._messageTabPage = New System.Windows.Forms.TabPage
        Me._additionalInfoTextBox = New System.Windows.Forms.TextBox
        Me._additionalInfoLabel = New System.Windows.Forms.Label
        Me._messageTextBox = New System.Windows.Forms.TextBox
        Me._exceptionMessageLabel = New System.Windows.Forms.Label
        Me._treeTabPage = New System.Windows.Forms.TabPage
        Me._exceptionTreeView = New System.Windows.Forms.TreeView
        Me._exceptionTreeViewLabel = New System.Windows.Forms.Label
        Me._stackTabPage = New System.Windows.Forms.TabPage
        Me._showAllStackCheckBox = New System.Windows.Forms.CheckBox
        Me._stackDataGrid = New System.Windows.Forms.DataGrid
        Me._stackListTextBox = New System.Windows.Forms.TextBox
        Me._stackListLabel = New System.Windows.Forms.Label
        Me._statusLabel = New System.Windows.Forms.Label
        Me._retryButton = New System.Windows.Forms.Button
        Me._continueButton = New System.Windows.Forms.Button
        Me._bottomTableLayoutPanel = New System.Windows.Forms.TableLayoutPanel
        Me._abortButton = New System.Windows.Forms.Button
        Me._toolTip = New System.Windows.Forms.ToolTip(Me.components)
        Me._mainTabControl.SuspendLayout()
        Me._messageTabPage.SuspendLayout()
        Me._treeTabPage.SuspendLayout()
        Me._stackTabPage.SuspendLayout()
        CType(Me._stackDataGrid, System.ComponentModel.ISupportInitialize).BeginInit()
        Me._bottomTableLayoutPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_mainTabControl
        '
        Me._mainTabControl.Controls.Add(Me._messageTabPage)
        Me._mainTabControl.Controls.Add(Me._treeTabPage)
        Me._mainTabControl.Controls.Add(Me._stackTabPage)
        Me._mainTabControl.Dock = System.Windows.Forms.DockStyle.Fill
        Me._mainTabControl.Location = New System.Drawing.Point(0, 0)
        Me._mainTabControl.Name = "_mainTabControl"
        Me._mainTabControl.SelectedIndex = 0
        Me._mainTabControl.Size = New System.Drawing.Size(463, 513)
        Me._mainTabControl.TabIndex = 0
        '
        '_messageTabPage
        '
        Me._messageTabPage.Controls.Add(Me._additionalInfoTextBox)
        Me._messageTabPage.Controls.Add(Me._additionalInfoLabel)
        Me._messageTabPage.Controls.Add(Me._messageTextBox)
        Me._messageTabPage.Controls.Add(Me._exceptionMessageLabel)
        Me._messageTabPage.Location = New System.Drawing.Point(4, 22)
        Me._messageTabPage.Name = "_messageTabPage"
        Me._messageTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._messageTabPage.Size = New System.Drawing.Size(455, 487)
        Me._messageTabPage.TabIndex = 0
        Me._messageTabPage.Text = "Exception Message"
        Me._messageTabPage.UseVisualStyleBackColor = True
        '
        '_additionalInfoTextBox
        '
        Me._additionalInfoTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._additionalInfoTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._additionalInfoTextBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._additionalInfoTextBox.ForeColor = System.Drawing.SystemColors.InfoText
        Me._additionalInfoTextBox.Location = New System.Drawing.Point(3, 258)
        Me._additionalInfoTextBox.Multiline = True
        Me._additionalInfoTextBox.Name = "_additionalInfoTextBox"
        Me._additionalInfoTextBox.ReadOnly = True
        Me._additionalInfoTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._additionalInfoTextBox.Size = New System.Drawing.Size(449, 226)
        Me._additionalInfoTextBox.TabIndex = 10
        '
        '_additionalInfoLabel
        '
        Me._additionalInfoLabel.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me._additionalInfoLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._additionalInfoLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._additionalInfoLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._additionalInfoLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me._additionalInfoLabel.Location = New System.Drawing.Point(3, 241)
        Me._additionalInfoLabel.Name = "_additionalInfoLabel"
        Me._additionalInfoLabel.Size = New System.Drawing.Size(449, 17)
        Me._additionalInfoLabel.TabIndex = 12
        Me._additionalInfoLabel.Text = " Additional Information"
        Me._additionalInfoLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_messageTextBox
        '
        Me._messageTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._messageTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._messageTextBox.Location = New System.Drawing.Point(3, 20)
        Me._messageTextBox.Multiline = True
        Me._messageTextBox.Name = "_messageTextBox"
        Me._messageTextBox.ReadOnly = True
        Me._messageTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._messageTextBox.Size = New System.Drawing.Size(449, 221)
        Me._messageTextBox.TabIndex = 8
        '
        '_exceptionMessageLabel
        '
        Me._exceptionMessageLabel.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me._exceptionMessageLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._exceptionMessageLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._exceptionMessageLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._exceptionMessageLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me._exceptionMessageLabel.Location = New System.Drawing.Point(3, 3)
        Me._exceptionMessageLabel.Name = "_exceptionMessageLabel"
        Me._exceptionMessageLabel.Size = New System.Drawing.Size(449, 17)
        Me._exceptionMessageLabel.TabIndex = 11
        Me._exceptionMessageLabel.Text = " Exception Message"
        Me._exceptionMessageLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_treeTabPage
        '
        Me._treeTabPage.Controls.Add(Me._exceptionTreeView)
        Me._treeTabPage.Controls.Add(Me._exceptionTreeViewLabel)
        Me._treeTabPage.Location = New System.Drawing.Point(4, 22)
        Me._treeTabPage.Name = "_treeTabPage"
        Me._treeTabPage.Padding = New System.Windows.Forms.Padding(3)
        Me._treeTabPage.Size = New System.Drawing.Size(455, 487)
        Me._treeTabPage.TabIndex = 1
        Me._treeTabPage.Text = "Exception Tree"
        Me._treeTabPage.UseVisualStyleBackColor = True
        '
        '_exceptionTreeView
        '
        Me._exceptionTreeView.BackColor = System.Drawing.SystemColors.Window
        Me._exceptionTreeView.Dock = System.Windows.Forms.DockStyle.Fill
        Me._exceptionTreeView.Location = New System.Drawing.Point(3, 20)
        Me._exceptionTreeView.Name = "_exceptionTreeView"
        Me._exceptionTreeView.Size = New System.Drawing.Size(449, 464)
        Me._exceptionTreeView.TabIndex = 13
        '
        '_exceptionTreeViewLabel
        '
        Me._exceptionTreeViewLabel.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me._exceptionTreeViewLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._exceptionTreeViewLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._exceptionTreeViewLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._exceptionTreeViewLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me._exceptionTreeViewLabel.Location = New System.Drawing.Point(3, 3)
        Me._exceptionTreeViewLabel.Name = "_exceptionTreeViewLabel"
        Me._exceptionTreeViewLabel.Size = New System.Drawing.Size(449, 17)
        Me._exceptionTreeViewLabel.TabIndex = 14
        Me._exceptionTreeViewLabel.Text = " Exception Tree"
        Me._exceptionTreeViewLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_stackTabPage
        '
        Me._stackTabPage.Controls.Add(Me._showAllStackCheckBox)
        Me._stackTabPage.Controls.Add(Me._stackDataGrid)
        Me._stackTabPage.Controls.Add(Me._stackListTextBox)
        Me._stackTabPage.Controls.Add(Me._stackListLabel)
        Me._stackTabPage.Location = New System.Drawing.Point(4, 22)
        Me._stackTabPage.Name = "_stackTabPage"
        Me._stackTabPage.Size = New System.Drawing.Size(455, 487)
        Me._stackTabPage.TabIndex = 2
        Me._stackTabPage.Text = "Exception Stack"
        Me._stackTabPage.UseVisualStyleBackColor = True
        '
        '_showAllStackCheckBox
        '
        Me._showAllStackCheckBox.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me._showAllStackCheckBox.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me._showAllStackCheckBox.Font = New System.Drawing.Font("Microsoft Sans Serif", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._showAllStackCheckBox.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me._showAllStackCheckBox.Location = New System.Drawing.Point(370, 185)
        Me._showAllStackCheckBox.Name = "_showAllStackCheckBox"
        Me._showAllStackCheckBox.Size = New System.Drawing.Size(80, 16)
        Me._showAllStackCheckBox.TabIndex = 13
        Me._showAllStackCheckBox.Text = "Show All"
        Me._showAllStackCheckBox.UseVisualStyleBackColor = False
        '
        '_stackDataGrid
        '
        Me._stackDataGrid.AlternatingBackColor = System.Drawing.SystemColors.Info
        Me._stackDataGrid.CaptionBackColor = System.Drawing.SystemColors.InactiveCaption
        Me._stackDataGrid.CausesValidation = False
        Me._stackDataGrid.DataMember = ""
        Me._stackDataGrid.Dock = System.Windows.Forms.DockStyle.Fill
        Me._stackDataGrid.GridLineColor = System.Drawing.SystemColors.Info
        Me._stackDataGrid.HeaderBackColor = System.Drawing.SystemColors.Info
        Me._stackDataGrid.HeaderForeColor = System.Drawing.SystemColors.ControlText
        Me._stackDataGrid.Location = New System.Drawing.Point(0, 183)
        Me._stackDataGrid.Name = "_stackDataGrid"
        Me._stackDataGrid.ParentRowsBackColor = System.Drawing.SystemColors.Window
        Me._stackDataGrid.PreferredColumnWidth = 150
        Me._stackDataGrid.ReadOnly = True
        Me._stackDataGrid.RowHeadersVisible = False
        Me._stackDataGrid.Size = New System.Drawing.Size(455, 304)
        Me._stackDataGrid.TabIndex = 10
        '
        '_stackListTextBox
        '
        Me._stackListTextBox.BackColor = System.Drawing.SystemColors.Window
        Me._stackListTextBox.Dock = System.Windows.Forms.DockStyle.Top
        Me._stackListTextBox.ForeColor = System.Drawing.SystemColors.ControlText
        Me._stackListTextBox.Location = New System.Drawing.Point(0, 17)
        Me._stackListTextBox.Multiline = True
        Me._stackListTextBox.Name = "_stackListTextBox"
        Me._stackListTextBox.ReadOnly = True
        Me._stackListTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Vertical
        Me._stackListTextBox.Size = New System.Drawing.Size(455, 166)
        Me._stackListTextBox.TabIndex = 11
        '
        '_stackListLabel
        '
        Me._stackListLabel.BackColor = System.Drawing.SystemColors.InactiveCaption
        Me._stackListLabel.Dock = System.Windows.Forms.DockStyle.Top
        Me._stackListLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._stackListLabel.Font = New System.Drawing.Font("Microsoft Sans Serif", 9.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._stackListLabel.ForeColor = System.Drawing.SystemColors.ActiveCaptionText
        Me._stackListLabel.Location = New System.Drawing.Point(0, 0)
        Me._stackListLabel.Name = "_stackListLabel"
        Me._stackListLabel.Size = New System.Drawing.Size(455, 17)
        Me._stackListLabel.TabIndex = 12
        Me._stackListLabel.Text = " Stack List"
        Me._stackListLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_statusLabel
        '
        Me._statusLabel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._statusLabel.Location = New System.Drawing.Point(3, 3)
        Me._statusLabel.Margin = New System.Windows.Forms.Padding(3)
        Me._statusLabel.Name = "_statusLabel"
        Me._statusLabel.Padding = New System.Windows.Forms.Padding(3)
        Me._statusLabel.Size = New System.Drawing.Size(265, 24)
        Me._statusLabel.TabIndex = 6
        Me._statusLabel.Text = "<status>"
        Me._statusLabel.TextAlign = System.Drawing.ContentAlignment.MiddleLeft
        '
        '_retryButton
        '
        Me._retryButton.DialogResult = System.Windows.Forms.DialogResult.Retry
        Me._retryButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me._retryButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._retryButton.Location = New System.Drawing.Point(343, 3)
        Me._retryButton.Name = "_retryButton"
        Me._retryButton.Size = New System.Drawing.Size(53, 24)
        Me._retryButton.TabIndex = 4
        Me._retryButton.Text = "&Retry"
        Me._toolTip.SetToolTip(Me._retryButton, "Close the exception display form and retry the same procedure that caused the exc" & _
                "eption")
        '
        '_continueButton
        '
        Me._continueButton.DialogResult = System.Windows.Forms.DialogResult.Ignore
        Me._continueButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me._continueButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._continueButton.Location = New System.Drawing.Point(402, 3)
        Me._continueButton.Name = "_continueButton"
        Me._continueButton.Size = New System.Drawing.Size(58, 24)
        Me._continueButton.TabIndex = 5
        Me._continueButton.Text = "&Continue"
        Me._toolTip.SetToolTip(Me._continueButton, "Click to close the exception display form and allow the program to continue")
        '
        '_bottomTableLayoutPanel
        '
        Me._bottomTableLayoutPanel.ColumnCount = 4
        Me._bottomTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._bottomTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._bottomTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._bottomTableLayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._bottomTableLayoutPanel.Controls.Add(Me._abortButton, 0, 0)
        Me._bottomTableLayoutPanel.Controls.Add(Me._continueButton, 2, 0)
        Me._bottomTableLayoutPanel.Controls.Add(Me._retryButton, 1, 0)
        Me._bottomTableLayoutPanel.Controls.Add(Me._statusLabel, 0, 0)
        Me._bottomTableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._bottomTableLayoutPanel.Location = New System.Drawing.Point(0, 513)
        Me._bottomTableLayoutPanel.Name = "_bottomTableLayoutPanel"
        Me._bottomTableLayoutPanel.RowCount = 1
        Me._bottomTableLayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._bottomTableLayoutPanel.Size = New System.Drawing.Size(463, 30)
        Me._bottomTableLayoutPanel.TabIndex = 2
        '
        '_abortButton
        '
        Me._abortButton.DialogResult = System.Windows.Forms.DialogResult.Abort
        Me._abortButton.Dock = System.Windows.Forms.DockStyle.Fill
        Me._abortButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._abortButton.Location = New System.Drawing.Point(274, 3)
        Me._abortButton.Name = "_abortButton"
        Me._abortButton.Size = New System.Drawing.Size(63, 24)
        Me._abortButton.TabIndex = 7
        Me._abortButton.Text = "&Abort"
        Me._toolTip.SetToolTip(Me._abortButton, "Close the exception display form and allow the program to continue")
        '
        'ExceptionDisplay
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(463, 543)
        Me.Controls.Add(Me._mainTabControl)
        Me.Controls.Add(Me._bottomTableLayoutPanel)
        Me.Name = "ExceptionDisplay"
        Me.Text = "Form1"
        Me._mainTabControl.ResumeLayout(False)
        Me._messageTabPage.ResumeLayout(False)
        Me._messageTabPage.PerformLayout()
        Me._treeTabPage.ResumeLayout(False)
        Me._stackTabPage.ResumeLayout(False)
        Me._stackTabPage.PerformLayout()
        CType(Me._stackDataGrid, System.ComponentModel.ISupportInitialize).EndInit()
        Me._bottomTableLayoutPanel.ResumeLayout(False)
        Me.ResumeLayout(False)

    End Sub
    Private WithEvents _mainTabControl As System.Windows.Forms.TabControl
    Private WithEvents _messageTabPage As System.Windows.Forms.TabPage
    Private WithEvents _treeTabPage As System.Windows.Forms.TabPage
    Private WithEvents _stackTabPage As System.Windows.Forms.TabPage
    Private WithEvents _messageTextBox As System.Windows.Forms.TextBox
    Private WithEvents _additionalInfoTextBox As System.Windows.Forms.TextBox
    Private WithEvents _exceptionMessageLabel As System.Windows.Forms.Label
    Private WithEvents _additionalInfoLabel As System.Windows.Forms.Label
    Private WithEvents _exceptionTreeView As System.Windows.Forms.TreeView
    Private WithEvents _exceptionTreeViewLabel As System.Windows.Forms.Label
    Private WithEvents _stackListLabel As System.Windows.Forms.Label
    Private WithEvents _stackDataGrid As System.Windows.Forms.DataGrid
    Private WithEvents _stackListTextBox As System.Windows.Forms.TextBox
    Private WithEvents _showAllStackCheckBox As System.Windows.Forms.CheckBox
    Private WithEvents _statusLabel As System.Windows.Forms.Label
    Private WithEvents _retryButton As System.Windows.Forms.Button
    Private WithEvents _continueButton As System.Windows.Forms.Button
    Private WithEvents _bottomTableLayoutPanel As System.Windows.Forms.TableLayoutPanel
    Private WithEvents _toolTip As System.Windows.Forms.ToolTip
    Private WithEvents _abortButton As System.Windows.Forms.Button
End Class
