''' <summary>
''' Displays exception information and message.
''' </summary>
''' <license>
''' (c) 2003 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="10/20/03" by="Katheleen Dollard" revision="1.0.1388.x">
''' created.
''' </history>
Public Class ExceptionDisplay

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    Public Sub New()
        MyBase.New()

        ' This call is required by the Windows Form Designer.
        InitializeComponent()

        ' Add any initialization after the InitializeComponent() call
        ' onInstantiate()

    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary>
    ''' Adds a drop shaddow parameter.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>
    ''' From Code Project: http://www.codeproject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)> _
        Get
            Dim cp As Windows.Forms.CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " METHODS and PROPERTIES "

    ''' <summary>Displays This form.</summary>
    ''' <param name="ex">Specifies the exception to display.</param>
    ''' <param name="buttons">Specifies what buttons to show.</param>
    ''' <remarks>This method defaults to the application XML hints file, if defined.</remarks>
    Public Overloads Function ShowDialog( _
                    ByVal ex As System.Exception, _
                    ByVal buttons As ExceptionDisplayButtons) _
                    As Windows.Forms.DialogResult

        If ex IsNot Nothing Then
            _xmlHintsFilePathName = Me.XmlHintsFilePathName
            _handledException = ex
            Me.Text = "Exception Display"

            'Dim abortCaption As String = "&Abort"
            ' Dim continueCaption As String = "&Continue"
            'Dim retryCaption As String = "&Retry"
            Dim ignoreCaption As String = "&Ignore"
            Select Case buttons
                Case ExceptionDisplayButtons.AbortContinue

                    Me._abortButton.Visible = True
                    Me._continueButton.Visible = True
                    Me._retryButton.Visible = False

                Case ExceptionDisplayButtons.AbortRetryIgnore

                    Me._continueButton.Text = ignoreCaption

                Case ExceptionDisplayButtons.Abort

                    Me._abortButton.Visible = True
                    Me._continueButton.Visible = False
                    Me._retryButton.Visible = False

                Case ExceptionDisplayButtons.Retry

                    Me._abortButton.Visible = False
                    Me._continueButton.Visible = False
                    Me._retryButton.Visible = True

                Case ExceptionDisplayButtons.Continue

                    Me._abortButton.Visible = False
                    Me._continueButton.Visible = True
                    Me._retryButton.Visible = False

                Case ExceptionDisplayButtons.None

                    Me._abortButton.Visible = False
                    Me._continueButton.Visible = False
                    Me._retryButton.Visible = False

            End Select

            Return Me.ShowDialog()
        Else
            Return Windows.Forms.DialogResult.None
        End If

    End Function

    ''' <summary>
    ''' Holds the XML file with additional information.
    ''' </summary>
    ''' <remarks></remarks>
    Private _xmlHints As Xml.XmlDocument

    Private _xmlHintsFilePathName As String = String.Empty
    ''' <summary>Gets or sets the name of the XML Hints file name.</summary>
    Public Property XmlHintsFilePathName() As String
        Get
            If String.IsNullOrEmpty(_xmlHintsFilePathName) Then
                Return System.Windows.Forms.Application.ExecutablePath & ".hints"
            Else
                Return _xmlHintsFilePathName
            End If
        End Get
        Set(ByVal value As String)
            _xmlHintsFilePathName = value
        End Set
    End Property

    Private _handledException As System.Exception
    ''' <summary>
    ''' Holds the exception to display.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property HandledException() As Exception
        Get
            Return _handledException
        End Get
        Set(ByVal value As Exception)
            _handledException = value
        End Set
    End Property

#End Region

#Region " FORM EVENT HANDLERS "

    ''' <summary>Occurs after the form is closed.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>This event is a notification that the form has already gone away before
    ''' control is returned to the calling method (in case of a modal form).  Use this
    ''' method to delete any temporary files that were created or dispose of any objects
    ''' not disposed with the closing event.
    ''' </remarks>
    Private Sub form_Closed(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles MyBase.Closed

    End Sub

    ''' <summary>Occurs before the form is closed</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.ComponentModel.CancelEventArgs"/></param>
    ''' <remarks>Use this method to allow canceling the closing of the form.
    '''   Because the form is not yet closed at this point, this is also the best 
    '''   place to serialize a form's visible properties, such as size and 
    '''   location.</remarks>
    Private Sub form_Closing(ByVal sender As System.Object, ByVal e As System.ComponentModel.CancelEventArgs) _
    Handles MyBase.Closing

    End Sub

    ''' <summary>Occurs when the form is loaded.</summary>
    ''' <param name="sender"><see cref="System.Object"/> instance of this 
    '''   <see cref="System.Windows.Forms.Form"/></param>
    ''' <param name="e"><see cref="System.EventArgs"/></param>
    ''' <remarks>Use this method for doing any final initialization right before 
    '''   the form is shown.  This is a good place to change the Visible and
    '''   ShowInTaskbar properties to start the form as hidden.  
    '''   Starting a form as hidden is useful for forms that need to be running but that
    '''   should not show themselves right away, such as forms with a notify icon in the
    '''   task bar.</remarks>
    Private Sub form_Load(ByVal sender As System.Object, ByVal e As System.EventArgs) _
    Handles MyBase.Load

        Try

            ' Turn on the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.WaitCursor

            ' instantiate form objects
            'Me.instantiateObjects()

            ' set the form caption
            ' Me.Text = Windows.Forms.Application.ProductVersion

            ' set tool tips
            'initializeUserInterface()

            ' center the form
            ' Me.CenterToScreen()

            ' turn on the loaded flag
            loaded = True

        Catch

            ' Use throw without an argument in order to preserve the stack location 
            ' where the exception was initially raised.
            Throw

        Finally

            ' Turn off the form hourglass cursor
            Me.Cursor = System.Windows.Forms.Cursors.Default

        End Try

    End Sub

#End Region

#Region " PRIVATE  and  PROTECTED "

    ''' <summary>Builds a column style for a data grid.</summary>
    ''' <param name="headerText"></param>
    ''' <param name="mappingName"></param>
    ''' <param name="width"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <history></history>
    Private Shared Function BuildColumnStyle(ByVal headerText As String, _
      ByVal mappingName As String, ByVal width As Int32) As Windows.Forms.DataGridColumnStyle

        Dim cs As New Windows.Forms.DataGridTextBoxColumn
        cs.HeaderText = headerText
        cs.MappingName = mappingName
        cs.Width = width
        Return cs

    End Function

    ''' <summary>
    ''' Returns additional information from the XML Hints file for 
    ''' the specified exception.</summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    ''' <history>
    ''' </history>
    Private Function findAdditionalInfo() As String

        Dim currentException As System.Exception = _handledException
        If _xmlHints Is Nothing Then
            Dim xmlHints As New Xml.XmlDocument
            If System.IO.File.Exists(Me.XmlHintsFilePathName) Then
                xmlHints.Load(Me.XmlHintsFilePathName)
                _xmlHints = xmlHints
            End If
        End If
        If _xmlHints Is Nothing Then
            ' Problem file not included with this assembly.
            Return "Problem file not included with this assembly."
        End If
        Dim nodeList As Xml.XmlNodeList
        Dim fullName As String
        Dim Int16Name As String
        Dim info As New System.Text.StringBuilder
        Do While currentException IsNot Nothing
            fullName = currentException.GetType.FullName
            Int16Name = currentException.GetType.Name
            nodeList = _xmlHints.SelectNodes(String.Format(Globalization.CultureInfo.CurrentCulture, _
                "//Hint[@FullName='{0}' or @Int16Name='{1}']/HintMessage", fullName, Int16Name))
            For i As Int32 = 0 To nodeList.Count - 1
                ' Dim node As Xml.XmlNode = CType(nodeList.Item(i), Xml.XmlNode)
                info.Append(nodeList.Item(i).InnerText)
                info.Append(Environment.NewLine)
            Next
            currentException = currentException.InnerException
        Loop
        If info.Length = 0 Then
            ' Problem clarification not provided for this exception.
            Return "Problem clarification not provided for this exception."
        Else
            Return info.ToString
        End If

    End Function

    Private _stackArray As StackEntry()
    Private _stackArrayInt16 As StackEntry()

    ''' <summary>
    ''' Adds a node to the exception tree.
    ''' </summary>
    ''' <param name="ex"></param>
    ''' <param name="t"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function loadTreeNode(ByVal ex As System.Exception, _
            ByVal t As Windows.Forms.TreeNodeCollection) As Windows.Forms.TreeNode

        Dim tnode As New Windows.Forms.TreeNode
        If ex IsNot Nothing Then
            tnode.Text = ex.GetType.Name
            tnode.Tag = ex
            t.Add(tnode)
            If ex.InnerException Is Nothing Then
                Return tnode
            Else
                Return loadTreeNode(ex.InnerException, tnode.Nodes)
            End If
        Else
            Return tnode
        End If
    End Function

    ''' <summary>
    ''' Returns the formatted message with details.
    ''' </summary>
    ''' <param name="ex">Specifies the exception.</param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function FormattedMessage(ByVal ex As Exception) As String

        If ex Is Nothing Then
            Return String.Empty
        End If
        Dim messageBuilder As New System.Text.StringBuilder
        messageBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "Message: {0}{1}", ex.Message, Environment.NewLine)
        messageBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}Type: {1}", Environment.NewLine, ex.GetType.FullName)
        messageBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}Program: {1}", Environment.NewLine, ex.Source)
        messageBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}Method: {1}", Environment.NewLine, ex.TargetSite)
        If ex.InnerException IsNot Nothing Then
            messageBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}Caused by: {1} Exception", Environment.NewLine, ex.InnerException.GetType.FullName)
            messageBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{For more info, select the causing exception from the Exception Tree)", Environment.NewLine)
        End If
        If ex.Data IsNot Nothing AndAlso ex.Data.Count > 0 Then
            For Each key As String In ex.Data.Keys
                messageBuilder.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{1}: {2}", Environment.NewLine, key, ex.Data.Item(key))
            Next
        End If
        Return messageBuilder.ToString

    End Function

    ''' <summary>
    ''' Selects a new exception from the tree and updates the grid and exceptions.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub selectNewExceptionTreeNode()

        Dim ex As System.Exception
        Dim node As Windows.Forms.TreeNode
        Dim view As Windows.Forms.TreeView = _exceptionTreeView
        Dim entryList As New System.Collections.ArrayList
        Dim Int16List As New System.Collections.ArrayList
        Dim entry As StackEntry
        Me.Text = "Exception Display"
        Try
            node = view.SelectedNode
            If node IsNot Nothing Then
                If TypeOf node.Tag Is System.Exception Then
                    ex = CType(node.Tag, System.Exception)
                    If ex IsNot Nothing Then
                        Me.Text = String.Format(Globalization.CultureInfo.CurrentCulture, _
                            "{0} Exception Display", ex.GetType.FullName)
                        Me._messageTextBox.Text = FormattedMessage(ex)
                        If String.IsNullOrEmpty(ex.StackTrace) Then
                            _stackListTextBox.Text = "<Empty Exception Trace>"
                        Else
                            _stackListTextBox.Text = ex.StackTrace
                            Dim stack As String() = ex.StackTrace.Split(Environment.NewLine.ToCharArray()(0))
                            For i As Int32 = 0 To stack.GetUpperBound(0)
                                entry = New StackEntry(stack(i))
                                entryList.Add(entry)
                                entry.RowNumber = entryList.Count
                                If Not entry.IsFramework Then
                                    Int16List.Add(entry)
                                End If
                            Next
                            If entryList.Count > 0 Then
                                _stackArray = CType(entryList.ToArray(GetType(StackEntry)), StackEntry())
                                If Int16List.Count > 0 Then
                                    _stackArrayInt16 = CType(Int16List.ToArray(GetType(StackEntry)), StackEntry())
                                    _stackDataGrid.SetDataBinding(_stackArrayInt16, String.Empty)
                                End If
                            End If
                        End If
                    End If
                End If
            End If
        Catch
            '        Diagnostics.Debug.WriteLine(exn)
            '       Debug.Assert(False)
            Throw
        End Try

    End Sub

    Private Sub setTableStyle()

        If _stackDataGrid.TableStyles.Count = 0 Then
            Dim ts As New Windows.Forms.DataGridTableStyle
            '.GridColumnStyles.Clear()
            ts.RowHeadersVisible = False
            ts.MappingName = "StackEntry[]"
            ts.GridColumnStyles.Add(ExceptionDisplay.BuildColumnStyle(String.Empty, "RowNumber", 30))
            ts.GridColumnStyles.Add(ExceptionDisplay.BuildColumnStyle("Method", "Method #", 100))
            ts.GridColumnStyles.Add(ExceptionDisplay.BuildColumnStyle("Line", "Position", 40))
            ts.GridColumnStyles.Add(ExceptionDisplay.BuildColumnStyle("File", "File", 100))
            ts.GridColumnStyles.Add(ExceptionDisplay.BuildColumnStyle("Call", "Method", 200))
            ts.GridColumnStyles.Add(ExceptionDisplay.BuildColumnStyle("Path", "FullFile", 200))
            _stackDataGrid.TableStyles.Add(ts)
            _stackDataGrid.Refresh()
        End If

    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>Is true after form has completed loading so that we can disable check boxes 
    '''   that should not fire until the form is fully loaded.</summary>
    Private loaded As Boolean

    ''' <summary>
    ''' Populates the exception tree and selects the first node. 
    ''' </summary>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Protected Overrides Sub OnLoad(ByVal e As System.EventArgs)
        setTableStyle()
        Dim nodeDeep As Windows.Forms.TreeNode = loadTreeNode(_handledException, _exceptionTreeView.Nodes)
        Me._additionalInfoTextBox.Text = Me.findAdditionalInfo()
        If Me._retryButton.Visible Then
            Me._statusLabel.Text = "Select Abort or Continue:"
        Else
            Me._statusLabel.Text = "Click Continue to close:"
        End If
        loaded = True
        _exceptionTreeView.SelectedNode = nodeDeep
        selectNewExceptionTreeNode()
    End Sub

    ''' <summary>
    ''' Selects whether to show all stack entries including the framework data.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub showAllStackCheckBox_CheckedChanged(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles _showAllStackCheckBox.CheckedChanged

        If loaded Then

            If _showAllStackCheckBox.Checked Then
                _stackDataGrid.SetDataBinding(_stackArray, String.Empty)
            Else
                _stackDataGrid.SetDataBinding(_stackArrayInt16, String.Empty)
            End If

        End If

    End Sub

    Private Sub exceptionTree_AfterSelect(ByVal sender As System.Object, ByVal e As System.Windows.Forms.TreeViewEventArgs) _
      Handles _exceptionTreeView.AfterSelect

        If loaded Then
            selectNewExceptionTreeNode()
        End If

    End Sub

    Private Sub stackDataGrid_MouseMove(ByVal sender As System.Object, ByVal e As System.Windows.Forms.MouseEventArgs) _
      Handles _stackDataGrid.MouseMove

        If Not loaded Then
            Return
        End If

        Dim hit As Windows.Forms.DataGrid.HitTestInfo
        Dim grid As Windows.Forms.DataGrid = CType(sender, System.Windows.Forms.DataGrid)
        If grid.HitTest(e.X, e.Y) IsNot Nothing Then
            hit = grid.HitTest(e.X, e.Y)
            If hit.Type = System.Windows.Forms.DataGrid.HitTestType.Cell Then
                If (hit.Row >= 0) AndAlso (hit.Row < _stackArray.Length) Then
                    Dim stackentry As StackEntry = _stackArray(hit.Row)
                    Dim memberName As String
                    memberName = grid.TableStyles(0).GridColumnStyles(hit.Column).MappingName()
                    Me._toolTip.SetToolTip(grid, stackentry.GetText(memberName))
                End If
            Else
                Me._toolTip.SetToolTip(grid, String.Empty)
            End If
        Else
            Me._toolTip.SetToolTip(grid, String.Empty)
        End If
    End Sub

    Private Sub continueButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) Handles _continueButton.Click

        Me.DialogResult = _continueButton.DialogResult
        Me.Close()

    End Sub

    Private Sub abortButton_Click(ByVal sender As System.Object, ByVal e As System.EventArgs) _
      Handles _retryButton.Click

        Me.DialogResult = _retryButton.DialogResult
        Me.Close()

    End Sub

#End Region

End Class

''' <summary>Handles the stack entry array element.</summary>
''' <history date="10/20/03" by="Katheleen Dollard" revision="1.0.1388.x">
'''  Create
''' </history>
''' <history date="10/20/03" by="David Hary" revision="1.0.1388.x">
'''   Modified
''' </history>
Friend Class StackEntry

    Public Sub New(ByVal line As String)
        If Not String.IsNullOrEmpty(line) Then
            ParseLine(line)
        End If
    End Sub

    Public ReadOnly Property IsFramework() As Boolean
        Get
            Return _position Is Nothing OrElse _position.Trim.Length = 0
        End Get
    End Property

    Public Function GetText(ByVal memberName As String) As String
        ' We could use reflection, but This in intended for tool tips
        ' and This is faster, even if ugly. 
        Select Case 0 ' memberName.ToLower(globalization.CultureInfo.CurrentCulture)
            Case String.Compare(memberName, "method", True, Globalization.CultureInfo.CurrentCulture)
                Return Method
            Case String.Compare(memberName, "methodInt16", True, Globalization.CultureInfo.CurrentCulture)
                Return MethodInt16
            Case String.Compare(memberName, "file", True, Globalization.CultureInfo.CurrentCulture)
                Return File
            Case String.Compare(memberName, "fullfile", True, Globalization.CultureInfo.CurrentCulture)
                Return FullFile
            Case String.Compare(memberName, "position", True, Globalization.CultureInfo.CurrentCulture)
                Return Position
            Case String.Compare(memberName, "rownumber", True, Globalization.CultureInfo.CurrentCulture)
                Return RowNumber.ToString(Globalization.CultureInfo.CurrentCulture)
            Case Else
                Return String.Empty
        End Select

    End Function

    Public Sub ParseLine(ByVal line As String)

        _method = String.Empty
        _methodInt16 = String.Empty
        _fullFile = String.Empty
        _position = String.Empty
        _file = String.Empty

        If line Is Nothing OrElse (line.Trim.Length = 0) Then
            Return
        End If
        line = line.Trim
        If line.StartsWith("at ", StringComparison.OrdinalIgnoreCase) Then
            line = line.Substring(3)
        End If
        If String.IsNullOrEmpty(line) Then
            Return
        End If
        Dim arr As String() = line.Split(")"c)
        ' Debug.WriteLine(line)
        _method = arr(0)
        _methodInt16 = StackEntry.SubstringAfter(StackEntry.SubstringBefore(arr(0), "("), ".")
        If arr.GetLength(0) > 1 Then
            line = arr(1).Trim
            If Not String.IsNullOrEmpty(line) Then
                If line.StartsWith("in ", StringComparison.OrdinalIgnoreCase) Then
                    line = line.Substring(3)
                End If
                _fullFile = StackEntry.SubstringBefore(line, ":").Trim
                _position = StackEntry.SubstringAfter(line, ":").Trim
                If _position IsNot Nothing AndAlso _position.StartsWith("line ", StringComparison.OrdinalIgnoreCase) Then
                    _position = _position.Substring(5)
                End If
                _file = StackEntry.SubstringBefore(StackEntry.SubstringAfter(line, "\"), ":").Trim
            End If
        End If

    End Sub

    Private _method As String
    Public Property Method() As String
        Get
            Return _method
        End Get
        Set(ByVal value As String)
            _method = value
        End Set
    End Property

    Private _rowNumber As Int32
    ''' <summary>Gets or sets the stack entry row number.
    ''' </summary>
    Public Property RowNumber() As Int32
        Get
            Return _rowNumber
        End Get
        Set(ByVal value As Int32)
            _rowNumber = value
        End Set
    End Property

    Private _methodInt16 As String
    Public Property MethodInt16() As String
        Get
            Return _methodInt16
        End Get
        Set(ByVal value As String)
            _methodInt16 = value
        End Set
    End Property

    Private _file As String
    Public Property File() As String
        Get
            Return _file
        End Get
        Set(ByVal value As String)
            _file = value
        End Set
    End Property

    Private _fullFile As String
    Public Property FullFile() As String
        Get
            Return _fullFile
        End Get
        Set(ByVal value As String)
            _fullFile = value
        End Set
    End Property

    Private _position As String
    Public Property Position() As String
        Get
            Return _position
        End Get
        Set(ByVal value As String)
            _position = value
        End Set
    End Property

    ''' <summary>Returns the substring after the specified string</summary>
    ''' <param name="s">The string to substring.</param>
    ''' <param name="c">The string to search for.</param>
    Private Shared Function SubstringAfter(ByVal s As String, ByVal c As String) As String

        Dim location As Integer = s.LastIndexOf(c, StringComparison.OrdinalIgnoreCase)
        If location >= 0 Then
            Return s.Substring(location + 1)
        Else
            Return String.Empty
        End If

    End Function

    ''' <summary>Returns the substring before the specified string</summary>
    ''' <param name="s">The string to substring.</param>
    ''' <param name="c">The string to search for.</param>
    Private Shared Function SubstringBefore(ByVal s As String, ByVal c As String) As String

        Dim location As Integer = s.LastIndexOf(c, StringComparison.OrdinalIgnoreCase)
        If location >= 0 Then
            Return s.Substring(0, location)
        Else
            Return String.Empty
        End If
    End Function

End Class

''' <summary>
''' Holds the button options for the exception display.
''' </summary>
<System.Flags()> Public Enum ExceptionDisplayButtons
    None = 0
    [Continue] = 1
    [Abort] = 2
    AbortContinue = 3
    [Retry] = 4
    AbortRetryIgnore = 7
End Enum
