Imports Microsoft.VisualBasic
Imports System
Imports System.Drawing
Imports System.Windows.Forms
Imports System.Security.Permissions

''' <summary>Form with fade in and out capabilities.</summary>
''' <history date="08/13/2007" by="Nicholas Seward" revision="1.0.2781.x">
''' http://www.codeproject.com/KB/cs/LetYourFormDropAShadow.aspx     
''' </history>
''' <history date="08/13/2007" by="David Hary" revision="1.0.2781.x">
''' Convert from C#
''' </history>
''' <remarks>
''' Features:  fades in on open; fades out on close; partially fades out on focus lost; 
''' fades in on focus; fades out on minimize; fades in on restore; 
''' must not annoy the user.
''' Usage:  To use FadeForm, just extend it instead of Form and you are ready to go.
''' It is defaulted to use the fade-in from nothing on open and out to nothing on close. 
''' It will also fade to 85% opacity when not the active window. 
''' Fading can be disabled and enabled with two methods setting the default enable and disable
''' modes.
''' </remarks>
Partial Public Class FadeForm
    Inherits Form

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Creates a new FadeForm.
    ''' </summary>
    <CodeAnalysis.SuppressMessage("Microsoft.Mobility", "CA1601:DoNotUseTimersThatPreventPowerStateChanges", Justification:="ok")> _
    Public Sub New()
        MyBase.New()
        Me.timer = New System.Windows.Forms.Timer()
        Me.SuspendLayout()
        Me.timer.Interval = 25
        AddHandler timer.Tick, AddressOf timer_Tick
        AddHandler Deactivate, AddressOf FadeForm_Deactivate
        AddHandler Activated, AddressOf FadeForm_Activated
        AddHandler Load, AddressOf FadeForm_Load
    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary>
    ''' Adds a drop shaddow parameter.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>
    ''' From Code Project: http://www.codeproject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)> _
        Get
            Dim cp As CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " VARIABLES "

    ''' <summary>
    ''' Timer to aid in fade effects.
    ''' </summary>
    Private timer As System.Windows.Forms.Timer

#End Region

#Region " PROPERTIES "

    ''' <summary>
    ''' Gets or sets the opacity that the form will transition to when the form gets focus.
    ''' </summary>
    Private _activeOpacity As Double = 1

    ''' <summary>
    ''' Gets or sets the opacity that the form will transition to when the form gets focus.
    ''' </summary>
    Public Property ActiveOpacity() As Double
        Get
            Return _activeOpacity
        End Get
        Set(ByVal value As Double)
            If value >= 0 AndAlso value <= 1 Then
                _activeOpacity = value
            Else
                Throw New ArgumentOutOfRangeException("value", "The Active Opacity must be between 0 and 1.")
            End If
            If Me.ContainsFocus Then
                Me.TargetOpacity = _activeOpacity
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the time it takes to fade from 0 to 1 or the other way around.
    ''' </summary>
    Private _fadeTime As Double = 0.35

    ''' <summary>
    ''' Gets or sets the time it takes to fade from 1 to 0 or the other way around.
    ''' </summary>
    Public Property FadeTime() As Double
        Get
            Return _fadeTime
        End Get
        Set(ByVal value As Double)
            If value > 0 Then
                _fadeTime = value
            Else
                Throw New ArgumentOutOfRangeException("value", "The FadeTime must be a positive value.")
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the opacity that the form will transition to when the form doesn't have focus.
    ''' </summary>
    Private _inactiveOpacity As Double = 0.85

    ''' <summary>
    ''' Gets or sets the opacity that the form will transition to when the form doesn't have focus.
    ''' </summary>
    Public Property InactiveOpacity() As Double
        Get
            Return _inactiveOpacity
        End Get
        Set(ByVal value As Double)
            If value >= 0 AndAlso value <= 1 Then
                _inactiveOpacity = value
            Else
                Throw New ArgumentOutOfRangeException("value", "The InactiveOpacity must be between 0 and 1.")
            End If
            If (Not Me.ContainsFocus) AndAlso Me.WindowState <> FormWindowState.Minimized Then
                Me.TargetOpacity = _inactiveOpacity
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the opacity that the form will transition to when the form is minimized.
    ''' </summary>
    Private _minimizedOpacity As Double

    ''' <summary>
    ''' Gets or sets the opacity that the form will transition to when the form is minimized.
    ''' </summary>
    Public Property MinimizedOpacity() As Double
        Get
            Return _minimizedOpacity
        End Get
        Set(ByVal value As Double)
            If value >= 0 AndAlso value <= 1 Then
                _minimizedOpacity = value
            Else
                Throw New ArgumentOutOfRangeException("value", "The MinimizedOpacity must be between 0 and 1.")
            End If
            If (Not Me.ContainsFocus) AndAlso Me.WindowState <> FormWindowState.Minimized Then
                Me.TargetOpacity = _inactiveOpacity
            End If
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the opacity the form is transitioning to.
    ''' </summary>
    Private _targetOpacity As Double

    ''' <summary>
    ''' Gets or sets the opacity the form is transitioning to.
    ''' </summary>
    ''' <remarks>
    ''' Setting this value amounts also to a one-time fade to any value.
    ''' The opacity is never actually 1. This is hack to keep the window from flashing black. 
    ''' The cause of this is not clear.
    ''' </remarks>
    Public Property TargetOpacity() As Double
        Get
            Return _targetOpacity
        End Get
        Set(ByVal value As Double)
            _targetOpacity = value
            If (Not timer.Enabled) Then
                timer.Start()
            End If
        End Set
    End Property

#End Region

#Region " METHODS "

    ''' <summary>
    ''' Turns off opacitiy fading.
    ''' </summary>
    Public Sub DisableFade()
        _activeOpacity = 1
        _inactiveOpacity = 1
        _minimizedOpacity = 1
    End Sub

    ''' <summary>
    ''' Turns on opacitiy fading.
    ''' </summary>
    Public Sub EnableFadeDefaults()
        _activeOpacity = 1
        _inactiveOpacity = 0.85
        _minimizedOpacity = 0
        _fadeTime = 0.35
    End Sub

#End Region

#Region " WINDOWS MESSAGES "

#Region " WindowsMessageCodes "
    Private Const WM_SYSCOMMAND As Integer = &H112
    Private Const WM_COMMAND As Integer = &H111
    Private Const SC_MINIMIZE As Integer = &HF020
    Private Const SC_RESTORE As Integer = &HF120
    Private Const SC_CLOSE As Integer = &HF060
#End Region

    ''' <summary>
    ''' Gets or sets the WindowsMessage that is being held until the end of a transition.
    ''' </summary>
    Private heldMessage As Message = New Message()

    ''' <summary>
    ''' Intercepts WindowMessages before they are processed.
    ''' </summary>
    ''' <param name="m">Windows Message</param>
    ''' <remarks>
    ''' The minimize and close events require messaging because these actions have to be 
    ''' postponed until the fade transition is complete. Overriding WndProc catches the 
    ''' request for those actions and postpones the action until the transition is done.
    ''' </remarks>
    <SecurityPermission(SecurityAction.LinkDemand, Flags:=SecurityPermissionFlag.UnmanagedCode)> _
    Protected Overrides Sub WndProc(ByRef m As Message)
        If m.Msg = WM_SYSCOMMAND OrElse m.Msg = WM_COMMAND Then
            'Fade to zero on minimze
            If m.WParam = New IntPtr(SC_MINIMIZE) Then
                If heldMessage.WParam <> New IntPtr(SC_MINIMIZE) Then
                    heldMessage = m
                    Me.TargetOpacity = _minimizedOpacity
                Else
                    heldMessage = New Message()
                    Me.TargetOpacity = _activeOpacity
                End If
                Return

                'Fade in if the window is restored from the taskbar
            ElseIf m.WParam = New IntPtr(SC_RESTORE) AndAlso Me.WindowState = FormWindowState.Minimized Then
                MyBase.WndProc(m)
                Me.TargetOpacity = _activeOpacity
                Return

                'Fade out if the window is closed.
            ElseIf m.WParam = New IntPtr(SC_CLOSE) Then
                heldMessage = m
                Me.TargetOpacity = _minimizedOpacity
                Return
            End If
        End If

        MyBase.WndProc(m)
    End Sub

#End Region

#Region " EVENT HANDLERS "

    ''' <summary>
    ''' Causes the form to fade in.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub FadeForm_Load(ByVal sender As Object, ByVal e As EventArgs)
        Me.Opacity = _minimizedOpacity
        Me.TargetOpacity = _activeOpacity
    End Sub

    ''' <summary>
    ''' Performs fade increment.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks>
    ''' The timer is stopped after reaching the target opacity. 
    ''' The timer gets started whenever the <see cref="TargetOpacity">target opacity</see> is 
    ''' set method. The timer is stopped to conserve processor power when not needed.
    ''' </remarks>
    Private Sub timer_Tick(ByVal sender As Object, ByVal e As EventArgs)

        Dim fadeChangePerTick As Double = timer.Interval * 1.0 / 1000 / _fadeTime

        'Check to see if it is time to stop the timer
        If Math.Abs(_targetOpacity - Me.Opacity) < fadeChangePerTick Then
            'There is an ugly black flash if you set the Opacity to 1.0
            If _targetOpacity = 1 Then
                MyBase.Opacity = 0.999
            Else
                MyBase.Opacity = _targetOpacity
            End If
            'Process held Windows Message.
            MyBase.WndProc(heldMessage)
            heldMessage = New Message()
            'Stop the timer to save processor.
            timer.Stop()
        ElseIf _targetOpacity > Me.Opacity Then
            MyBase.Opacity += fadeChangePerTick
        ElseIf _targetOpacity < Me.Opacity Then
            MyBase.Opacity -= fadeChangePerTick
        End If
    End Sub

    'Fade out the form when it losses focus.
    Private Sub FadeForm_Deactivate(ByVal sender As Object, ByVal e As EventArgs)
        Me.TargetOpacity = _inactiveOpacity
    End Sub

    'Fade in the form when it gaines focus.
    Private Sub FadeForm_Activated(ByVal sender As Object, ByVal e As EventArgs)
        Me.TargetOpacity = _activeOpacity
    End Sub

#End Region

End Class

