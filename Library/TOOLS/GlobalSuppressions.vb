Imports System.Diagnostics.CodeAnalysis

#Region "CA1020:AvoidNamespacesWithFewTypes"
' FxCop says that namespaces should generally have more than five types.
' Unfortunately, not all of these namespaces currently have more than five
' types but we still want the namespace so we can expand the library in the
' future without moving types around. 
<Assembly: SuppressMessage("Microsoft.Design", "CA1020:AvoidNamespacesWithFewTypes", _
    Scope:="namespace", Target:="isr.WindowsForms", _
    Justification:="Ignoring this warning...we want these namespaces, but don't have enough classes to go in them to satisfy the rule.")> 
#End Region

#Region " INSTRUCTIONS "

' This module provides assembly level (global) CodeAnalysis suppressions for FxCop.

' While static code analysis with FxCop is excellent for catching many common
' and not so common code errors, there are some things that it flags that
' do not always apply to the project at hand. For those cases, FxCop allows
' you to exclude the message (and optionally give a justification reason for
' excluding it). However, those exclusions are stored only in the FxCop
' project file. In the 2.0 version of the .NET framework, Microsoft introduced
' SuppressMessageAttribute, which is used primarily by the version of FxCop
' that is built in to Visual Studio. As this built-in functionality is not
' included in all versions of Visual Studio, we have opted to continue
' using the standalone version of FxCop. 

' In order for this version to recognize SupressMessageAttribute, the
' CODE_ANALYSIS symbol must be defined as follows:

' In Visual Studio Non-Team system you have to enable your project to recognize the 
' SuppressMessage attribute by FIRST adding a condition compilation symbol.
' 1. In Solution Explorer, right-click your project and choose Properties
' 2. In the Properties window, choose the Compile tab and click Advanced Compile Options
' 3. In the Custom constants text box enter CODE_ANALYSIS

#End Region
