<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class MessageDialog

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub
    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer
    Private WithEvents _cancelButton As System.Windows.Forms.Button
    Private WithEvents _oKButton As System.Windows.Forms.Button
    Private WithEvents _noButton As System.Windows.Forms.Button
    Private WithEvents _yesButton As System.Windows.Forms.Button
    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me._cancelButton = New System.Windows.Forms.Button
        Me._oKButton = New System.Windows.Forms.Button
        Me._noButton = New System.Windows.Forms.Button
        Me._yesButton = New System.Windows.Forms.Button
        Me._ButtonsLayout = New System.Windows.Forms.TableLayoutPanel
        Me._messageTextBox = New System.Windows.Forms.TextBox
        Me._LayoutPanel = New System.Windows.Forms.TableLayoutPanel
        Me._ProgressBar = New System.Windows.Forms.ProgressBar
        Me._Timer = New System.Windows.Forms.Timer(Me.components)
        Me._ButtonsLayout.SuspendLayout()
        Me._LayoutPanel.SuspendLayout()
        Me.SuspendLayout()
        '
        '_cancelButton
        '
        Me._cancelButton.BackColor = System.Drawing.SystemColors.Control
        Me._cancelButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._cancelButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._cancelButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._cancelButton.Location = New System.Drawing.Point(373, 3)
        Me._cancelButton.Name = "_cancelButton"
        Me._cancelButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._cancelButton.Size = New System.Drawing.Size(105, 45)
        Me._cancelButton.TabIndex = 4
        Me._cancelButton.Text = "&CANCEL"
        Me._cancelButton.UseVisualStyleBackColor = True
        '
        '_oKButton
        '
        Me._oKButton.BackColor = System.Drawing.SystemColors.Control
        Me._oKButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._oKButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._oKButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._oKButton.Location = New System.Drawing.Point(262, 3)
        Me._oKButton.Name = "_oKButton"
        Me._oKButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._oKButton.Size = New System.Drawing.Size(105, 45)
        Me._oKButton.TabIndex = 0
        Me._oKButton.Text = "&OK"
        Me._oKButton.UseVisualStyleBackColor = True
        '
        '_noButton
        '
        Me._noButton.BackColor = System.Drawing.SystemColors.Control
        Me._noButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._noButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._noButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._noButton.Location = New System.Drawing.Point(40, 3)
        Me._noButton.Name = "_noButton"
        Me._noButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._noButton.Size = New System.Drawing.Size(105, 45)
        Me._noButton.TabIndex = 2
        Me._noButton.Text = "&NO"
        Me._noButton.UseVisualStyleBackColor = True
        '
        '_yesButton
        '
        Me._yesButton.BackColor = System.Drawing.SystemColors.Control
        Me._yesButton.Cursor = System.Windows.Forms.Cursors.Default
        Me._yesButton.Font = New System.Drawing.Font("Arial", 9.75!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._yesButton.ForeColor = System.Drawing.SystemColors.ControlText
        Me._yesButton.Location = New System.Drawing.Point(151, 3)
        Me._yesButton.Name = "_yesButton"
        Me._yesButton.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._yesButton.Size = New System.Drawing.Size(105, 45)
        Me._yesButton.TabIndex = 1
        Me._yesButton.Text = "&YES"
        Me._yesButton.UseVisualStyleBackColor = True
        '
        '_ButtonsLayout
        '
        Me._ButtonsLayout.ColumnCount = 6
        Me._ButtonsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ButtonsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._ButtonsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._ButtonsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._ButtonsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle)
        Me._ButtonsLayout.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0!))
        Me._ButtonsLayout.Controls.Add(Me._noButton, 1, 0)
        Me._ButtonsLayout.Controls.Add(Me._yesButton, 2, 0)
        Me._ButtonsLayout.Controls.Add(Me._cancelButton, 4, 0)
        Me._ButtonsLayout.Controls.Add(Me._oKButton, 3, 0)
        Me._ButtonsLayout.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ButtonsLayout.Location = New System.Drawing.Point(6, 431)
        Me._ButtonsLayout.Name = "_ButtonsLayout"
        Me._ButtonsLayout.RowCount = 1
        Me._ButtonsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._ButtonsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51.0!))
        Me._ButtonsLayout.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 51.0!))
        Me._ButtonsLayout.Size = New System.Drawing.Size(519, 49)
        Me._ButtonsLayout.TabIndex = 7
        '
        '_messageTextBox
        '
        Me._messageTextBox.BackColor = System.Drawing.SystemColors.Info
        Me._messageTextBox.Dock = System.Windows.Forms.DockStyle.Fill
        Me._messageTextBox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._messageTextBox.Location = New System.Drawing.Point(6, 3)
        Me._messageTextBox.Multiline = True
        Me._messageTextBox.Name = "_messageTextBox"
        Me._messageTextBox.ScrollBars = System.Windows.Forms.ScrollBars.Both
        Me._messageTextBox.Size = New System.Drawing.Size(519, 393)
        Me._messageTextBox.TabIndex = 8
        '
        '_LayoutPanel
        '
        Me._LayoutPanel.ColumnCount = 3
        Me._LayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._LayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._LayoutPanel.ColumnStyles.Add(New System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 3.0!))
        Me._LayoutPanel.Controls.Add(Me._messageTextBox, 1, 0)
        Me._LayoutPanel.Controls.Add(Me._ButtonsLayout, 1, 2)
        Me._LayoutPanel.Controls.Add(Me._ProgressBar, 1, 1)
        Me._LayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill
        Me._LayoutPanel.Location = New System.Drawing.Point(0, 0)
        Me._LayoutPanel.Name = "_LayoutPanel"
        Me._LayoutPanel.RowCount = 3
        Me._LayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100.0!))
        Me._LayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._LayoutPanel.RowStyles.Add(New System.Windows.Forms.RowStyle)
        Me._LayoutPanel.Size = New System.Drawing.Size(531, 483)
        Me._LayoutPanel.TabIndex = 9
        '
        '_ProgressBar
        '
        Me._ProgressBar.Dock = System.Windows.Forms.DockStyle.Bottom
        Me._ProgressBar.Location = New System.Drawing.Point(6, 402)
        Me._ProgressBar.Name = "_ProgressBar"
        Me._ProgressBar.Size = New System.Drawing.Size(519, 23)
        Me._ProgressBar.TabIndex = 9
        '
        '_Timer
        '
        Me._Timer.Interval = 1000
        '
        'MessageDialog
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(7.0!, 14.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.BackColor = System.Drawing.SystemColors.Control
        Me.ClientSize = New System.Drawing.Size(531, 483)
        Me.ControlBox = False
        Me.Controls.Add(Me._LayoutPanel)
        Me.Cursor = System.Windows.Forms.Cursors.Default
        Me.Font = New System.Drawing.Font("Arial", 8.25!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ForeColor = System.Drawing.SystemColors.WindowText
        Me.KeyPreview = True
        Me.Location = New System.Drawing.Point(297, 264)
        Me.MinimizeBox = False
        Me.Name = "MessageDialog"
        Me.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ShowInTaskbar = False
        Me.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen
        Me.Text = "Select Datalog Option"
        Me._ButtonsLayout.ResumeLayout(False)
        Me._LayoutPanel.ResumeLayout(False)
        Me._LayoutPanel.PerformLayout()
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents _ButtonsLayout As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents _messageTextBox As System.Windows.Forms.TextBox
    Friend WithEvents _LayoutPanel As System.Windows.Forms.TableLayoutPanel
    Friend WithEvents _ProgressBar As System.Windows.Forms.ProgressBar
    Friend WithEvents _Timer As System.Windows.Forms.Timer

End Class
