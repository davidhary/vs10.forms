Imports System.Windows.Forms

''' <summary>
''' A message dialog.</summary>
''' <license>
''' (c) 1995 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history>
''' 12/07/98  David Hary  Update to change message fonts and use OK or Yes/No types
''' </history>
''' <history date="03/01/08" by="David Hary" revision="1.2.2982.x">
''' Converted to VS2008
''' </history>
''' <history date="08/25/09" by="David Hary" revision="1.2.3524.x">
''' Made non-singleton
''' </history>
''' <history date="10/25/10" by="David Hary" revision="1.2.3950.x">
''' Adds timeout.
''' </history>
''' <history date="08/22/1995" by="David Hary" revision="1.0.0.x">
''' created.
''' </history>
Public Class MessageDialog
    Inherits System.Windows.Forms.Form

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call
        ' Me.MessageFontSize = 10
        Me._dialogButtons = MessageBoxButtons.OK

    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary>
    ''' Adds a drop shaddow parameter.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>
    ''' From Code Project: http://www.codeproject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)> _
        Get
            Dim cp As Windows.Forms.CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " PROPERTIES "

    Public Property Caption() As String
        Get
            Return Me.Text
        End Get
        Set(ByVal value As String)
            Me.Text = value
        End Set
    End Property

    Private _isSpecialStyle As Boolean
    Public Property IsSpecialStyle() As Boolean
        Get
            Return _isSpecialStyle
        End Get
        Set(ByVal value As Boolean)
            _isSpecialStyle = value
        End Set
    End Property

    Private _defaultButton As MessageBoxDefaultButton
    Public Property DefaultButton() As MessageBoxDefaultButton
        Get
            Return _defaultButton
        End Get
        Set(ByVal value As MessageBoxDefaultButton)
            _defaultButton = value
        End Set
    End Property

    Private _dialogButtons As MessageBoxButtons
    Public Property DialogButtons() As MessageBoxButtons
        Get
            Return _dialogButtons
        End Get
        Set(ByVal Value As MessageBoxButtons)
            _dialogButtons = Value
        End Set
    End Property

    Public Property SpecialButtons() As DialogSpecialButton
        Get
            Return MessageDialog.ParseSpecialButtons(Me._dialogButtons)
        End Get
        Set(ByVal Value As DialogSpecialButton)
            Me._dialogButtons = ParseStandardStyle(Value)
        End Set
    End Property

    ''' <summary>
    ''' Gets or sets the message to display.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property Message() As String
        Get
            Return _messageTextBox.Text
        End Get
        Set(ByVal Value As String)
            If Me.IsSpecialStyle AndAlso True Then
                Value = Value & Environment.NewLine & "Click:"
                Value = Value & Environment.NewLine & "REPLACE to use the same name thus replacing the exisitng file; or"
                Value = Value & Environment.NewLine & "APPEND to append to the existing file; or"
                Value = Value & Environment.NewLine & "RENAME to select a new name for the data log file."
            End If
            _messageTextBox.Text = Value
            _messageTextBox.Refresh()
        End Set
    End Property

    Private _timeoutInterval As Integer
    ''' <summary>
    ''' Gets or sets the timeout interval in milliseconds. The dialog will close and return the 
    ''' default reply if the timeout elapsed.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property TimeoutInterval() As Integer
        Get
            Return _timeoutInterval
        End Get
        Set(ByVal value As Integer)
            _timeoutInterval = value
        End Set
    End Property
#End Region

#Region " SHOW METHODS "

    ''' <summary>
    ''' Prepare the form for display.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub prepareDisplay()
        Me._messageTextBox.Width = Me._initialWidth
        Me.setButtons()
        Me.setDefaultButton()
        Me.setTimeout()
    End Sub

    Public Overloads Sub Show(ByVal owner As Windows.Forms.IWin32Window, ByVal message As String)
        Me.IsSpecialStyle = False
        Me.Message = message
        Me.prepareDisplay()
        MyBase.Show(owner)
    End Sub

    Public Overloads Sub Show(ByVal message As String)
        Me.IsSpecialStyle = False
        Me.Message = message
        Me.prepareDisplay()
        MyBase.Show()
    End Sub

    Public Overloads Sub Show(ByVal message As String, ByVal caption As String)
        Me.Caption = caption
        Me.Show(message)
    End Sub

    Public Overloads Function ShowDialog(ByVal owner As Windows.Forms.IWin32Window, ByVal message As String) As Windows.Forms.DialogResult
        Me.IsSpecialStyle = False
        Me.Message = message
        Me.prepareDisplay()
        Return MyBase.ShowDialog(owner)
    End Function

    Public Overloads Function ShowDialog(ByVal message As String) As Windows.Forms.DialogResult
        Me.IsSpecialStyle = False
        Me.Message = message
        Me.prepareDisplay()
        Return MyBase.ShowDialog()
    End Function

    Public Overloads Function ShowDialog(ByVal message As String, ByVal caption As String, ByVal buttons As MessageBoxButtons) As Windows.Forms.DialogResult
        Me.DialogButtons = buttons
        Me.Caption = caption
        Me.ShowDialog(message)
    End Function

    Public Function ShowSpecialDialog(ByVal owner As Windows.Forms.IWin32Window, ByVal message As String) As DialogSpecialResult
        Me.IsSpecialStyle = True
        Me.Message = message
        Me.prepareDisplay()
        Return MessageDialog.ParseSpecialResult(MyBase.ShowDialog(owner))
    End Function

    Public Function ShowSpecialDialog(ByVal message As String) As DialogSpecialResult
        Me.IsSpecialStyle = True
        Me.Message = message
        Me.prepareDisplay()
        Return MessageDialog.ParseSpecialResult(MyBase.ShowDialog())
    End Function

    Public Function ShowSpecialDialog(ByVal message As String, ByVal caption As String, ByVal buttons As DialogSpecialButton) As DialogSpecialResult
        Me.Caption = caption
        Me.SpecialButtons = buttons
        Return Me.ShowSpecialDialog(message)
    End Function

    Public Function ShowSpecialDialog(ByVal owner As Windows.Forms.IWin32Window, ByVal message As String, ByVal caption As String, ByVal buttons As DialogSpecialButton) As DialogSpecialResult
        Me.IsSpecialStyle = True
        Me.SpecialButtons = buttons
        Me.Caption = caption
        Me.Message = message
        Me.prepareDisplay()
        Return MessageDialog.ParseSpecialResult(MyBase.ShowDialog(owner))
    End Function

#End Region

#Region " ARRANGEMENT METHODS "

    ' the loading flag
    Private _isLoading As Boolean

    ' The margins for the message.
    Private _defaultHorizontalMargin As Integer
    Private _defaulVerticalMargin As Integer
    Private _initialWidth As Integer

    ''' <summary>
    ''' Change the specified font size.
    ''' </summary>
    ''' <param name="font"></param>
    ''' <param name="size"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Shared Function changeFontSize(ByVal font As Drawing.Font, ByVal size As Single) As Drawing.Font
        Return New Drawing.Font(font.FontFamily, size, font.Style, font.Unit)
    End Function

    ''' <summary>
    ''' Sets the font size for the form. This will change the size of the form.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property FormFontSize() As Single
        Get
            Return Me._messageTextBox.Font.Size
        End Get
        Set(ByVal Value As Single)
            Me.Font = changeFontSize(Me.Font, Value)
        End Set
    End Property

    ''' <summary>
    ''' Sets the font size for the message.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property MessageFontSize() As Single
        Get
            Return Me._messageTextBox.Font.Size
        End Get
        Set(ByVal Value As Single)
            Me._messageTextBox.Font = changeFontSize(Me._messageTextBox.Font, Value)
        End Set
    End Property

    ''' <summary>
    ''' Sets the font size for controls.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Property ControlsFontSize() As Single
        Get
            Return Me._messageTextBox.Font.Size
        End Get
        Set(ByVal Value As Single)
            Me._cancelButton.Font = changeFontSize(Me._cancelButton.Font, Value)
            Me._oKButton.Font = changeFontSize(Me._oKButton.Font, Value)
            Me._noButton.Font = changeFontSize(Me._noButton.Font, Value)
            Me._yesButton.Font = changeFontSize(Me._yesButton.Font, Value)
        End Set
    End Property

    ''' <summary>
    ''' Converts the standard result to special result.
    ''' </summary>
    ''' <param name="standardResult"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ParseSpecialResult(ByVal standardResult As Windows.Forms.DialogResult) As DialogSpecialResult
        Return CType(CInt(standardResult), DialogSpecialResult)
    End Function

    ''' <summary>
    ''' Converts the standard buttons to special buttons.
    ''' </summary>
    ''' <param name="standardButtons"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ParseSpecialButtons(ByVal standardButtons As MessageBoxButtons) As DialogSpecialButton
        Return CType(CInt(standardButtons), DialogSpecialButton)
    End Function

    ''' <summary>
    ''' Converts the special buttons to standard buttons.
    ''' </summary>
    ''' <param name="specialButtons"></param>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Public Shared Function ParseStandardStyle(ByVal specialButtons As DialogSpecialButton) As MessageBoxButtons
        Return CType(CInt(specialButtons), MessageBoxButtons)
    End Function

    Private Sub resizeButton(ByVal button As System.Windows.Forms.Button, ByVal graphics As System.Drawing.Graphics)

        Dim minimumMargin As Integer = 4

        If button.Visible Then
            Const minimumHorizontalMarginPixels As Integer = 6
            Const minimumVerticalMarginPixels As Integer = 3

            Dim buttonText As String = button.Text

            Dim buttonWidth As Single = graphics.MeasureString(buttonText, button.Font).Width + 2 * minimumHorizontalMarginPixels
            If button.Width < buttonWidth Then
                button.Width = CInt(buttonWidth)
            End If

            Dim buttonHeight As Single = graphics.MeasureString(buttonText, button.Font).Height + 2 * minimumVerticalMarginPixels
            If button.Height < buttonHeight Then
                button.Height = CInt(buttonHeight)
            End If
        End If

        ' adjust the height of the container.
        Me._ButtonsLayout.Height = button.Height + minimumMargin

    End Sub

    ''' <summary>
    ''' Adjusts the message to fit the form.
    ''' </summary>
    ''' <param name="graphics"></param>
    ''' <remarks></remarks>
    ''' <history>
    ''' 12/07/98  David Hary  Update to change message fonts and use OK or Yes/No types
    ''' </history>
    Private Sub adjustMessage(ByVal graphics As System.Drawing.Graphics)

        ' resize the buttons
        resizeButton(_cancelButton, graphics)
        resizeButton(_oKButton, graphics)
        resizeButton(_noButton, graphics)
        resizeButton(_yesButton, graphics)

        ' prevent the resize command from working
        _isLoading = True

        ' make sure the message label is set to contain the message
        _messageTextBox.Refresh()
        System.Windows.Forms.Application.DoEvents()

        ' Set size of form as per size of message
        Me.Width = _messageTextBox.Width + _defaultHorizontalMargin + Me.Width - Me.ClientRectangle.Width
        If Me.Width < 4 * (_oKButton.Width + _defaultHorizontalMargin) Then
            Me.Width = 4 * (_oKButton.Width + _defaultHorizontalMargin)
        End If

        Me.Height = Me._messageTextBox.Top + Me._messageTextBox.Height + Me._ButtonsLayout.Height + 2 * _defaulVerticalMargin + Me.Height - Me.ClientRectangle.Height
        If Me.Height >= 0.95 * System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height Then
            Me.Height = CInt(0.95 * System.Windows.Forms.Screen.PrimaryScreen.Bounds.Height)
        End If

        If Me.Width >= 0.95 * System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width Then
            Me.Width = CInt(0.95 * System.Windows.Forms.Screen.PrimaryScreen.Bounds.Width)
        End If

        System.Windows.Forms.Application.DoEvents()

        Me.CenterToScreen()

        ' Center the label on the form.
        _messageTextBox.SetBounds(CInt(0.5 * (Me.Width - _messageTextBox.Width)), 0, 0, 0, Windows.Forms.BoundsSpecified.X)

        System.Windows.Forms.Application.DoEvents()

        ' Allow the resize command to work.
        _isLoading = False

    End Sub

    Private Sub setDefaultButton()

        ' Now that the form is visible set the default buttons.
        Select Case _dialogButtons

            Case MessageBoxButtons.OK

                Me.AcceptButton = _oKButton

            Case MessageBoxButtons.OKCancel

                If (Me._defaultButton And MessageBoxDefaultButton.Button1) <> 0 Then
                    Me.AcceptButton = _oKButton
                Else
                    Me.AcceptButton = _cancelButton
                End If

            Case MessageBoxButtons.RetryCancel

                If (Me._defaultButton And MessageBoxDefaultButton.Button1) <> 0 Then
                    Me.AcceptButton = _yesButton
                Else
                    Me.AcceptButton = _noButton
                End If

            Case MessageBoxButtons.YesNo

                If (Me._defaultButton And MessageBoxDefaultButton.Button1) <> 0 Then
                    Me.AcceptButton = _yesButton
                Else
                    Me.AcceptButton = _noButton
                End If

            Case MessageBoxButtons.YesNoCancel

                If (Me._defaultButton And MessageBoxDefaultButton.Button1) <> 0 Then
                    Me.AcceptButton = _yesButton
                ElseIf (Me._defaultButton And MessageBoxDefaultButton.Button2) <> 0 Then
                    Me.AcceptButton = _noButton
                Else
                    Me.AcceptButton = _cancelButton
                End If

            Case Else

                Me.AcceptButton = _oKButton

        End Select

        System.Windows.Forms.Application.DoEvents()

    End Sub

    Private Sub setButtons()

        ' hide the buttons
        Me._yesButton.Visible = False
        Me._noButton.Visible = False
        Me._cancelButton.Visible = False
        Me._oKButton.Visible = False

        ' select which buttons are to be displayed
        Select Case Me._dialogButtons

            Case MessageBoxButtons.YesNo

                ' display the Yes button
                _yesButton.Visible = True
                _yesButton.Text = "&YES"

                ' display the No button
                _noButton.Visible = True
                _noButton.Text = "&NO"

            Case MessageBoxButtons.OKCancel

                ' display the OK button
                _oKButton.Visible = True
                _oKButton.Text = "&OK"

                ' display the No button
                _cancelButton.Visible = True
                _cancelButton.Text = "&CANCEL"

            Case MessageBoxButtons.YesNoCancel

                If Me.IsSpecialStyle Then

                    ' display the Append button
                    _yesButton.Visible = True
                    _yesButton.Text = "&APPEND"

                    ' display the Rename button
                    _noButton.Visible = True
                    _noButton.Text = "&RENAME"

                    ' display the Replace button
                    _cancelButton.Visible = True
                    _cancelButton.Text = "RE&PLACE"

                Else

                    ' display the Append button
                    _yesButton.Visible = True
                    _yesButton.Text = "&YES"

                    ' display the Rename button
                    _noButton.Visible = True
                    _noButton.Text = "&NOE"

                    ' display the Replace button
                    _cancelButton.Visible = True
                    _cancelButton.Text = "&CANCEL"

                End If

            Case Else

                ' display the OK button
                _oKButton.Visible = True
                _oKButton.Text = "&OK"

        End Select
        System.Windows.Forms.Application.DoEvents()

    End Sub

    ''' <summary>
    ''' Returns the time left.
    ''' </summary>
    ''' <returns></returns>
    ''' <remarks></remarks>
    Private Function timeLeft() As Integer
        Return CInt(_endtime.Subtract(DateTime.Now).TotalMilliseconds)
    End Function

    Private _endtime As DateTime
    ''' <summary>
    ''' Sets the times and initiates the progress bar. 
    ''' </summary>
    ''' <remarks></remarks>
    <CodeAnalysis.SuppressMessage("Microsoft.Mobility", "CA1601:DoNotUseTimersThatPreventPowerStateChanges")> _
    Private Sub setTimeout()
        _endtime = DateTime.Now.Add(TimeSpan.FromMilliseconds(Me.TimeoutInterval))
        If Me.TimeoutInterval > 0 Then
            Me._ProgressBar.Visible = True
            Me._Timer.Interval = 250
            Me._Timer.Enabled = True
            Me._ProgressBar.Maximum = Me.TimeoutInterval
            Me.updateProgress()
        Else
            Me._ProgressBar.Visible = False
        End If
    End Sub

#End Region

#Region " FORM EVENT HANDLERS "

    Private Sub MessageDialog_FormClosing(ByVal sender As Object, ByVal e As System.Windows.Forms.FormClosingEventArgs) Handles Me.FormClosing
        Me._Timer.Enabled = False
    End Sub

    Private Sub form_Load(ByVal sender As Object, ByVal e As System.EventArgs) Handles MyBase.Load

        ' set the horizontal margin for the message
        _defaultHorizontalMargin = 4

        ' set the vertical margin for the message
        _defaulVerticalMargin = 8

        ' save the current message width
        Me._initialWidth = Me.Width - _defaultHorizontalMargin

    End Sub

    Private Sub form_Paint(ByVal sender As Object, ByVal e As System.Windows.Forms.PaintEventArgs) Handles Me.Paint

        If Not _isLoading Then

            ' resize the message box
            Me._messageTextBox.Width = Me.ClientRectangle.Width - _defaultHorizontalMargin

            ' reactive the form
            adjustMessage(e.Graphics)

        End If

    End Sub

#End Region

#Region " CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Closes and returns the <see cref="Windows.Forms.DialogResult.Cancel">Cancel</see>
    ''' dialog result.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub _cancelButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _cancelButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary>
    ''' Closes and returns the <see cref="Windows.Forms.DialogResult.OK">OK</see>
    ''' dialog result.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub _okButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _oKButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' Closes and returns the <see cref="Windows.Forms.DialogResult.No">NO</see>
    ''' dialog result.
    ''' </summary>
    ''' <remarks></remarks>
    Private Sub _noButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _noButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.No
        Me.Close()
    End Sub

    ''' <summary>
    ''' Closes and returns the <see cref="Windows.Forms.DialogResult.Yes">TES</see>
    ''' dialog result.
    ''' </summary>
    Private Sub _yesButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _yesButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.No
        Me.Close()
    End Sub

    Private Sub displayProgressText(ByVal value As String)
        Using gr As System.Drawing.Graphics = _ProgressBar.CreateGraphics()
            gr.DrawString(value, _
                          System.Drawing.SystemFonts.DefaultFont, _
                          System.Drawing.Brushes.Black, _
                          New System.Drawing.PointF(CSng(_ProgressBar.Width / 2 - (gr.MeasureString(value, System.Drawing.SystemFonts.DefaultFont).Width / 2.0F)), CSng(_ProgressBar.Height / 2 - (gr.MeasureString(value, System.Drawing.SystemFonts.DefaultFont).Height / 2.0F))))
        End Using
    End Sub

    Private Sub updateProgress()
        Me._ProgressBar.Value = timeLeft()
        'Dim percent As Integer = CInt(Fix((CDbl(_ProgressBar.Value - _ProgressBar.Minimum) / CDbl(_ProgressBar.Maximum - _ProgressBar.Minimum)) * 100))
        displayProgressText(String.Format(Globalization.CultureInfo.CurrentCulture, "DIALOG WILL CLOSE IN {0:0.000}S", timeLeft() / 1000))
    End Sub

    Private Sub _Timer_Tick(ByVal sender As Object, ByVal e As System.EventArgs) Handles _Timer.Tick
        Me.updateProgress()
        If timeLeft() <= 0 Then
            Me._Timer.Enabled = False
            Me.AcceptButton.PerformClick()
        End If
    End Sub

#End Region

End Class

#Region " TYPES "


''' <summary>
''' Enumerates the special results.  These results map onto the 
''' <see cref="Windows.Forms.DialogResult">standard dialgo result</see>.
''' </summary>
''' <remarks></remarks>
Public Enum DialogSpecialResult
    None = Windows.Forms.DialogResult.None
    [Append] = Windows.Forms.DialogResult.Yes
    [Rename] = Windows.Forms.DialogResult.No
    [Replace] = Windows.Forms.DialogResult.Cancel
End Enum

''' <summary>
''' Enumerates the special results.  These results map onto the 
''' <see cref="Windows.Forms.DialogResult">standard dialgo result</see>.
''' </summary>
''' <remarks></remarks>
Public Enum DialogSpecialButton
    None = Windows.Forms.MessageBoxButtons.OK
    AppendRenameReplace = Windows.Forms.MessageBoxButtons.YesNoCancel
End Enum

#End Region
