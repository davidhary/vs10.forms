<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class InputBox
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)

        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try

    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.components = New System.ComponentModel.Container
        Me._enteredValueTextBox = New System.Windows.Forms.TextBox
        Me._cancelButton = New System.Windows.Forms.Button
        Me._acceptButton = New System.Windows.Forms.Button
        Me._enteredValueTextBoxLabel = New System.Windows.Forms.Label
        Me._validationErrorProvider = New System.Windows.Forms.ErrorProvider(Me.components)
        CType(Me._validationErrorProvider, System.ComponentModel.ISupportInitialize).BeginInit()
        Me.SuspendLayout()
        '
        '_enteredValueTextBox
        '
        Me._enteredValueTextBox.Location = New System.Drawing.Point(8, 21)
        Me._enteredValueTextBox.Name = "_enteredValueTextBox"
        Me._enteredValueTextBox.Size = New System.Drawing.Size(160, 20)
        Me._enteredValueTextBox.TabIndex = 28
        Me._enteredValueTextBox.Text = "TextBox1"
        '
        '_cancelButton
        '
        Me._cancelButton.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me._cancelButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._cancelButton.Location = New System.Drawing.Point(8, 45)
        Me._cancelButton.Name = "_cancelButton"
        Me._cancelButton.Size = New System.Drawing.Size(75, 23)
        Me._cancelButton.TabIndex = 27
        Me._cancelButton.Text = "&Cancel"
        '
        '_acceptButton
        '
        Me._acceptButton.Enabled = False
        Me._acceptButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._acceptButton.Location = New System.Drawing.Point(93, 45)
        Me._acceptButton.Name = "_acceptButton"
        Me._acceptButton.Size = New System.Drawing.Size(75, 23)
        Me._acceptButton.TabIndex = 26
        Me._acceptButton.Text = "&OK"
        '
        '_enteredValueTextBoxLabel
        '
        Me._enteredValueTextBoxLabel.BackColor = System.Drawing.SystemColors.Control
        Me._enteredValueTextBoxLabel.Cursor = System.Windows.Forms.Cursors.Default
        Me._enteredValueTextBoxLabel.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me._enteredValueTextBoxLabel.Font = New System.Drawing.Font("Arial", 8.0!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me._enteredValueTextBoxLabel.ForeColor = System.Drawing.SystemColors.WindowText
        Me._enteredValueTextBoxLabel.Location = New System.Drawing.Point(8, 5)
        Me._enteredValueTextBoxLabel.Name = "_enteredValueTextBoxLabel"
        Me._enteredValueTextBoxLabel.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me._enteredValueTextBoxLabel.Size = New System.Drawing.Size(134, 16)
        Me._enteredValueTextBoxLabel.TabIndex = 25
        Me._enteredValueTextBoxLabel.Text = "Enter a Value: "
        Me._enteredValueTextBoxLabel.TextAlign = System.Drawing.ContentAlignment.BottomLeft
        '
        '_validationErrorProvider
        '
        Me._validationErrorProvider.ContainerControl = Me
        '
        'InputBox
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(179, 75)
        Me.ControlBox = False
        Me.Controls.Add(Me._enteredValueTextBox)
        Me.Controls.Add(Me._cancelButton)
        Me.Controls.Add(Me._acceptButton)
        Me.Controls.Add(Me._enteredValueTextBoxLabel)
        Me.Name = "InputBox"
        Me.Text = "InputBox"
        CType(Me._validationErrorProvider, System.ComponentModel.ISupportInitialize).EndInit()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Private WithEvents _enteredValueTextBox As System.Windows.Forms.TextBox
    Private WithEvents _cancelButton As System.Windows.Forms.Button
    Private WithEvents _acceptButton As System.Windows.Forms.Button
    Private WithEvents _enteredValueTextBoxLabel As System.Windows.Forms.Label
    Private WithEvents _validationErrorProvider As System.Windows.Forms.ErrorProvider
End Class
