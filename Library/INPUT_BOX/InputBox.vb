''' <summary>A data entry form.</summary>
''' <license>
''' (c) 2006 Integrated Scientific Resources, Inc.
''' Licensed under the Apache License Version 2.0. 
''' Unless required by applicable law or agreed to in writing, this software is provided
''' "AS IS", WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
''' </license>
''' <history date="04/05/06" by="David Hary" revision="1.0.2286.x">
''' Convert to VS2005
''' </history>
''' <history date="09/05/09" by="David Hary" revision="1.2.3535.x">
''' Removed inglton instance.
''' </history>
''' <history date="02/20/2006" by="David Hary" revision="1.0.2242.x">
''' created.
''' </history>
Public Class InputBox

#Region " CONSTRUCTORS  and  DESTRUCTORS "

    ''' <summary>
    ''' Construtor for this class.
    ''' </summary>
    ''' <remarks></remarks>
    Public Sub New()
        MyBase.New()

        'This call is required by the Windows Form Designer.
        InitializeComponent()

        'Add any initialization after the InitializeComponent() call

    End Sub

#Region " DROP SHADOW "

    ''' <summary>
    ''' Defines the Drop Shadow constant.
    ''' </summary>
    ''' <remarks></remarks>
    Private Const CS_DROPSHADOW As Integer = 131072

    ''' <summary>
    ''' Adds a drop shaddow parameter.
    ''' </summary>
    ''' <value></value>
    ''' <returns></returns>
    ''' <remarks>
    ''' From Code Project: http://www.codeproject.com/KB/cs/LetYourFormDropAShadow.aspx
    ''' </remarks>
    Protected Overrides ReadOnly Property CreateParams() As System.Windows.Forms.CreateParams
        <System.Security.Permissions.SecurityPermission(System.Security.Permissions.SecurityAction.LinkDemand, Flags:=System.Security.Permissions.SecurityPermissionFlag.UnmanagedCode)> _
        Get
            Dim cp As Windows.Forms.CreateParams = MyBase.CreateParams
            cp.ClassStyle = cp.ClassStyle Or CS_DROPSHADOW
            Return cp
        End Get
    End Property

#End Region

#End Region

#Region " PROPERTIES "

    ''' <summary>Returns the entered value.</summary>
    Public Property EnteredValue() As String
        Get
            Return Me._enteredValueTextBox.Text
        End Get
        Set(ByVal Value As String)
            Me._enteredValueTextBox.Text = Value
        End Set
    End Property

    Private _numberStyle As Globalization.NumberStyles = Globalization.NumberStyles.None
    ''' <summary>Gets or sets the number style requested.
    ''' </summary>
    Public Property NumberStyle() As Globalization.NumberStyles
        Get
            Return _numberStyle
        End Get
        Set(ByVal Value As Globalization.NumberStyles)
            _numberStyle = Value
        End Set
    End Property

#End Region

#Region " FORM AND CONTROL EVENT HANDLERS "

    ''' <summary>
    ''' Closes and returns the <see cref="Windows.Forms.DialogResult.OK">OK</see>
    ''' dialog result.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub okButton_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _acceptButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.OK
        Me.Close()
    End Sub

    ''' <summary>
    ''' Closes and returns the <see cref="Windows.Forms.DialogResult.Cancel">Cancel</see>
    ''' dialog result.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub cancelButton1_Click(ByVal sender As Object, ByVal e As System.EventArgs) Handles _cancelButton.Click
        Me.DialogResult = Windows.Forms.DialogResult.Cancel
        Me.Close()
    End Sub

    ''' <summary>
    ''' Validates the entered value.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub EnteredValueTextBox_Validating(ByVal sender As Object, ByVal e As System.ComponentModel.CancelEventArgs) Handles _enteredValueTextBox.Validating
        Me._acceptButton.Enabled = False
        Me._validationErrorProvider.SetError(Me._enteredValueTextBox, String.Empty)
        If Not Me.NumberStyle = Globalization.NumberStyles.None Then
            Dim outcome As Double
            e.Cancel = Not Double.TryParse(_enteredValueTextBox.Text, Me.NumberStyle, _
              Globalization.CultureInfo.CurrentCulture, outcome)
        End If
    End Sub

    ''' <summary>
    ''' Enables the OK button.
    ''' </summary>
    ''' <param name="sender"></param>
    ''' <param name="e"></param>
    ''' <remarks></remarks>
    Private Sub EnteredValueTextBox_Validated(ByVal sender As Object, ByVal e As System.EventArgs) Handles _enteredValueTextBox.Validated
        Me._acceptButton.Enabled = True
    End Sub

#End Region

End Class
