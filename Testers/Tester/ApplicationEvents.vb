Namespace My

    ''' <summary>
    ''' Handles application events.
    ''' </summary>
    ''' <history date="03/14/06" by="David Hary" revision="1.0.2264.x">
    ''' (c) 2010 Integrated Scientific Resources, Inc.
    ''' </history>
    ''' <history>
    ''' Created
    ''' </history>
    Partial Friend Class MyApplication

#Region " APPLICATION LOG "

        ''' <summary>
        ''' Adds a log message and severity to the log.
        ''' </summary>
        ''' <param name="severity">Specifies the message severity.</param>
        ''' <param name="details">Specifies the message details</param>
        ''' <returns>Message or empty string.</returns>
        ''' <remarks></remarks>
        Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal details As String) As String
            If details IsNot Nothing Then
                My.Application.Log.WriteEntry(details, severity)
                Return details
            End If
            Return ""
        End Function

        ''' <summary>
        ''' Adds a log message and severity to the log.
        ''' </summary>
        ''' <param name="severity">Specifies the message severity.</param>
        ''' <param name="format">Specifies the message format</param>
        ''' <param name="args">Specified the message arguments</param>
        ''' <remarks></remarks>
        Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal format As String, ByVal ParamArray args() As Object) As String
            If format IsNot Nothing Then
                Return WriteLogEntry(severity, String.Format(Globalization.CultureInfo.CurrentCulture, format, args))
            End If
            Return ""
        End Function

        ''' <summary>
        ''' Adds a log message and severity to the log.
        ''' </summary>
        ''' <param name="severity">Specifies the message severity.</param>
        ''' <param name="messages">Message information to log.</param>
        ''' <remarks></remarks>
        Public Shared Function WriteLogEntry(ByVal severity As TraceEventType, ByVal messages As String()) As String
            If messages IsNot Nothing Then
                Return WriteLogEntry(severity, String.Join(",", messages))
            End If
            Return ""
        End Function

        ''' <summary>
        ''' Adds exception details to the error log.  Includes stack and data.
        ''' </summary>
        ''' <param name="ex">Specifies the exception.</param>
        ''' <param name="severity">Specifies the exception severity.</param>
        ''' <param name="additionalInfo">Specifies additional information.</param>
        ''' <remarks></remarks>
        Private Shared Sub _writeExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, _
            ByVal additionalInfo As String)

            My.Application.Log.WriteException(ex, severity, additionalInfo)
            If ex IsNot Nothing AndAlso ex.StackTrace IsNot Nothing Then
                Dim stackTrace As String() = ex.StackTrace.Split(CChar(Environment.NewLine))
                WriteLogEntry(severity, stackTrace)
            End If
            If ex.Data IsNot Nothing AndAlso ex.Data.Count > 0 Then
                For Each keyValuePair As System.Collections.DictionaryEntry In ex.Data
                    My.Application.Log.WriteEntry(keyValuePair.Key.ToString & "=" & keyValuePair.Value.ToString, severity)
                Next
            End If
            If ex.InnerException IsNot Nothing Then
                _writeExceptionDetails(ex.InnerException, severity, "(Inner Exception)")
            End If

        End Sub

        ''' <summary>
        ''' Adds exception details to the error log.
        ''' </summary>
        ''' <param name="ex">Specifies the exception.</param>
        ''' <param name="severity">Specifies the exception severity.</param>
        ''' <param name="additionalInfo">Specifies additional information.</param>
        ''' <remarks></remarks>
        Public Shared Sub WriteExceptionDetails(ByVal ex As Exception, ByVal severity As TraceEventType, _
            ByVal additionalInfo As String)

            ' write exception details.
            _writeExceptionDetails(ex, severity, additionalInfo)

        End Sub

        ''' <summary>
        ''' Adds exception details to the error log.
        ''' </summary>
        ''' <param name="ex">Specifies the exception.</param>
        ''' <remarks></remarks>
        Public Shared Sub WriteExceptionDetails(ByVal ex As Exception)
            WriteExceptionDetails(ex, TraceEventType.Error, String.Empty)
        End Sub

#End Region

#Region " APPLICATION LEVEL METHODS AND PROPERTIES "

        Private Shared _currentProcessName As String
        ''' <summary>
        ''' Gets the current process name.  Also sets the desing mode.
        ''' </summary>
        ''' <value></value>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared ReadOnly Property CurrentProcessName() As String
            Get
                If String.IsNullOrEmpty(_currentProcessName) Then
                    _currentProcessName = Process.GetCurrentProcess().ProcessName.ToUpperInvariant
                    If _currentProcessName.ToUpperInvariant.Contains(".VSHOST") Then
                        My.MyApplication._inDesignMode = True
                    End If
                End If
                Return _currentProcessName
            End Get
        End Property

        Private Shared _inDesignMode As Boolean
        ''' <summary>Gets or sets the condition for running within the IDE, i.e., in Design Mode.</summary>
        ''' <value><c>InDesignMode</c> is a <see cref="Boolean"/> property that can be read from (read only).</value>
        ''' <remarks>Use this property to check if the application is running from the IDE.</remarks>
        Public Shared ReadOnly Property InDesignMode() As Boolean
            Get
#If DEBUG Then
                If Not My.MyApplication._inDesignMode AndAlso String.IsNullOrEmpty(My.MyApplication._currentProcessName) Then
                    _currentProcessName = My.MyApplication.CurrentProcessName
                End If
#End If
                Return My.MyApplication._inDesignMode
            End Get
        End Property

        ''' <summary>
        ''' Process any unhandled exceptions that occur in the application. 
        ''' Call this method from UI entry points in the application, such as button 
        ''' click events, when an unhandled exception occurs.  
        ''' This could also handle the Application.ThreadException event, however 
        ''' the VS2005 debugger breaks before the event Application.ThreadException 
        ''' is called.
        ''' </summary>
        ''' <param name="ex">Specifies the unhandled exception.</param>
        ''' <param name="buttons">Specifies the buttons on the exception display.</param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Public Shared Function ProcessException(ByVal ex As Exception, ByVal additionalInfo As String, ByVal buttons As isr.WindowsForms.ExceptionDisplayButtons) As Windows.Forms.DialogResult

            Dim result As Windows.Forms.DialogResult
            Try

                ' log the exception
                My.MyApplication.WriteExceptionDetails(ex, TraceEventType.Critical, additionalInfo)

                Dim frm As New isr.WindowsForms.ExceptionDisplay
                result = frm.ShowDialog(ex, buttons)
                My.Application.Log.WriteEntry(String.Format(Globalization.CultureInfo.CurrentCulture, _
                    "{0} requested by user.", result), TraceEventType.Verbose)

            Catch displayException As System.Exception

                ' Log but also display the error in a message box
                My.MyApplication.WriteExceptionDetails(displayException, TraceEventType.Critical, "Exception occurred displaying application exception.")

                Dim errorMessage As System.Text.StringBuilder = New System.Text.StringBuilder()
                errorMessage.Append("The following error occured while displaying the application exception:")
                errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}{1}", Environment.NewLine, displayException.Message)
                errorMessage.AppendFormat(Globalization.CultureInfo.CurrentCulture, "{0}{0}Click Abort to exit application.  Otherwise, the aplication will continue.", Environment.NewLine)
                result = MessageBox.Show( _
                  errorMessage.ToString(), "Application Error", MessageBoxButtons.AbortRetryIgnore, MessageBoxIcon.Stop)

            End Try

            Return result

        End Function

        Private Shared _usingDevices As Boolean
        ''' <summary>
        ''' Gets or sets the condition for using devices.
        ''' </summary>
        ''' <remarks>
        ''' When true, the application connects to actual devices.  Set to false in case
        ''' testing the application in the absence of devices.
        ''' </remarks>
        Public Shared Property UsingDevices() As Boolean
            Get
                Return _usingDevices
            End Get
            Set(ByVal value As Boolean)
                _usingDevices = value
            End Set
        End Property


#End Region

#Region " MODULE LEVEL METHODS AND PROPERTIES "

        ''' <summary>
        ''' Parses the command line.
        ''' </summary>
        ''' <returns>True if success or false if requesting to terminate.</returns>
        ''' <remarks></remarks>
        Private Function parseCommandLine(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Try

                ' process the project-specific command line
                _usingDevices = True
                For Each commandLineArg As String In commandLineArgs
                    If commandLineArg.StartsWith("/hw:", StringComparison.OrdinalIgnoreCase) Then
                        Dim value As String = commandLineArg.Substring(4)
                        _usingDevices = Not value.StartsWith("n", StringComparison.OrdinalIgnoreCase)
                    End If
                Next

                Return True

            Catch ex As System.Exception

                ex.Data.Add("Details", "Exception occurred processing command line arguments. Program execution will continue after this message.")

                ' log the exception
                MyApplication.WriteExceptionDetails(ex, TraceEventType.Critical, "Exception occurred processing command line.")

                Try

                    Dim frm As New isr.WindowsForms.ExceptionDisplay
                    Return frm.ShowDialog(ex, isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) <> DialogResult.Abort

                Catch exLocal As System.Exception

                    ' Log but also display the error in a message box
                    MyApplication.WriteExceptionDetails(exLocal, TraceEventType.Critical, "Exception occurred displaying error information (1).")
                    MessageBox.Show(exLocal.Message, _
                        Windows.Forms.Application.ProductName, MessageBoxButtons.OK, MessageBoxIcon.Error)

                    Return True

                End Try

            End Try

        End Function

        ''' <summary>Instantiates the application to its known state.
        ''' </summary>
        ''' <returns>True if success or false if requesting to terminate.</returns>
        ''' <remarks></remarks>
        Private Function initializeKnownState(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Try

                ' set application starting cursor
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting
                Return True

            Catch ex As System.Exception

                Return MyApplication.ProcessException(ex, "Exception occurred instantiating objects.", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) <> DialogResult.Abort

            Finally

                ' restore the default mouse cursor
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try

        End Function

        ''' <summary>Reads settings.
        ''' </summary>
        ''' <returns>True if success or false if requesting to terminate.</returns>
        ''' <remarks></remarks>
        Private Function readSettings() As Boolean

            Try

                ' read the project-specific settings

                ' restore the default mouse cursor
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default
                Return True

            Catch ex As System.Exception

                Return My.MyApplication.ProcessException(ex, "Exception occurred reading settings.", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) <> DialogResult.Abort

            End Try

        End Function

#End Region

#Region " APPLICATION EVENTS "

        ''' <summary>Occurs when the network connection is connected or disconnected.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub MyApplication_NetworkAvailabilityChanged(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.Devices.NetworkAvailableEventArgs) Handles Me.NetworkAvailabilityChanged

        End Sub

        ''' <summary>Raised after all application forms are closed.  
        ''' This event is not raised if the application terminates abnormally.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks>
        ''' The event saves user settings for all related libraries.
        ''' </remarks>
        Private Sub MyApplication_Shutdown(ByVal sender As Object, ByVal e As System.EventArgs) Handles Me.Shutdown

            ' flush the log.
            My.Application.Log.DefaultFileLogWriter.Flush()

            ' terminate all the project-specific objects
            ' Gopher.TerminateObjects()

            If My.Application.SaveMySettingsOnExit Then

                ' save settings on all related libraries. This is required to presist any new settings changed by the library.
                ' isr.Support.My.MyLibrary.SaveSettings()

                ' For some reason the event handling set in the Settings class dos not really work.
                My.Settings.Save()

            End If

            ' do some garbage collection
            System.GC.Collect()

        End Sub

        ''' <summary>Occurs when the application starts, before the startup form is created.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub MyApplication_Startup(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupEventArgs) Handles Me.Startup

            ' Turn on the screen hourglass
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting
            My.Application.DoEvents()

            ' add handler to handle the thread application.  This IDE does that only for run time
            ' this gets override by the IDE. 
            ' AddHandler Application.UnhandledException, AddressOf MyApplication_UnhandledException
            If isr.Core.IOExtensions.FileSize(My.Application.Log.DefaultFileLogWriter.FullLogFileName) < 2 Then
                Trace.CorrelationManager.StartLogicalOperation(My.Application.Info.AssemblyName)
                My.MyApplication.WriteLogEntry(TraceEventType.Critical, "{0} version {1} {2} {3}", My.Application.Info.ProductName, My.Application.Info.Version.ToString(4), Date.Now.ToShortDateString(), Date.Now.ToLongTimeString)
                Trace.CorrelationManager.StopLogicalOperation()
            End If

            Try

                If Me.parseCommandLine(e.CommandLine) AndAlso Me.initializeKnownState(e.CommandLine) AndAlso Me.readSettings Then

                    If My.MyApplication.InDesignMode Then
                        My.MyApplication.WriteLogEntry(TraceEventType.Verbose, "Design mode")
                    Else
                        My.MyApplication.WriteLogEntry(TraceEventType.Verbose, "Runtime mode")
                    End If

                Else

                    My.MyApplication.WriteLogEntry(TraceEventType.Error, "failed starting")
                    ' exit with an error code
                    e.Cancel = True
                    Environment.Exit(-1)
                    Windows.Forms.Application.Exit()

                End If

            Catch

                Throw

            Finally

                ' restore the default mouse cursor
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default

            End Try

        End Sub

        ''' <summary>Occurs when launching a single-instance application and the application is already active. 
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks></remarks>
        Private Sub MyApplication_StartupNextInstance(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.StartupNextInstanceEventArgs) Handles Me.StartupNextInstance

            My.MyApplication.WriteLogEntry(TraceEventType.Verbose, "starting next instant")

        End Sub

        ''' <summary>Raised if the application encounters an unhandled exception.
        ''' </summary>
        ''' <param name="sender"></param>
        ''' <param name="e"></param>
        ''' <remarks>Use this method to trap unhandled exceptions.  The Application.UnhandledException 
        '''   event fires whenever an unhandled exception is thrown on the current thread.  Use this
        '''   global exception handler to protect all forms from any unhandled errors. The handler
        '''   may ignore the error, log it to a file, display a message box that asks the end user 
        '''   whether she wants to abort the application, send an e-mail to the tech support group, 
        '''   and any other action you deem desirable.
        ''' </remarks>
        Private Sub MyApplication_UnhandledException(ByVal sender As Object, ByVal e As Microsoft.VisualBasic.ApplicationServices.UnhandledExceptionEventArgs) Handles Me.UnhandledException

            If My.MyApplication.ProcessException(e.Exception, "Unhandled Exception.", isr.WindowsForms.ExceptionDisplayButtons.AbortContinue) _
                  = Windows.Forms.DialogResult.Abort Then
                ' exit with an error code
                Environment.Exit(-1)
                e.ExitApplication = True
            End If

        End Sub

#End Region

#Region " ON EVENTS  "

        ''' <summary>
        ''' Replaces the default trace listener with the modified listener.
        ''' Updates the minimum splash screen display time.
        ''' </summary>
        ''' <param name="commandLineArgs"></param>
        ''' <returns></returns>
        ''' <remarks></remarks>
        Protected Overrides Function OnInitialize(ByVal commandLineArgs As System.Collections.ObjectModel.ReadOnlyCollection(Of String)) As Boolean

            Return MyBase.OnInitialize(commandLineArgs)

        End Function

#End Region

    End Class

End Namespace

