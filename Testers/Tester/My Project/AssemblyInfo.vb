Imports System.Reflection
Imports System.Runtime.InteropServices
Imports System.Security.Permissions

' General Information about an assembly is controlled through the following 
' set of attributes. Change these attribute values to modify the information
' associated with an assembly.

' Review the values of the assembly attributes

<Assembly: AssemblyTitle("ISR Windows Forms Library Tester")> 
<Assembly: AssemblyDescription("ISR Windows Forms Library Tester")> 
<Assembly: AssemblyCompany("Integrated Scientific Resources, Inc.")> 
<Assembly: AssemblyProduct("ISR Windows Forms VB Library 1.2")>
<Assembly: AssemblyCopyright("(c) 2009 Integrated Scientific Resources, Inc. All rights reserved.")>  
<Assembly: AssemblyTrademark("isr.WindowsForms and the ISR logo are trademarks of Integrated Scientific Resources, Inc.")> 
<Assembly: CLSCompliant(True)> 
<Assembly: Resources.NeutralResourcesLanguage("en-US", Resources.UltimateResourceFallbackLocation.MainAssembly)> 

' the following Key File is for the strong name signature of this assembly
'<Assembly: AssemblyKeyFileAttribute("\\muscat\\c\projects\keyPairs\isr\keyPair.snk")> 

' Request named permission set: Nothing, Execution, FullTrust, Internet, LocalIntranet, and SkipVerification. 
<Assembly: PermissionSetAttribute(SecurityAction.RequestMinimum, Name:="FullTrust")> 

' Disable accessibility of an individual managed type or member, or of all types within an assembly, to COM.
<Assembly: System.Runtime.InteropServices.ComVisible(False)> 

' The following GUID is for the ID of the type library if this project is exposed to COM
' <Assembly: Guid("AC42133E-D9AE-46AF-B1C3-4C8DA535CB16")> 

' Version information for an assembly consists of the following four values:
'
'      Major Version
'      Minor Version 
'      Build Number
'      Revision
'
' You can specify all the values or you can default the Build and Revision Numbers 
' by using the '*' as shown below:

<Assembly: AssemblyVersion("1.2.*")> 
