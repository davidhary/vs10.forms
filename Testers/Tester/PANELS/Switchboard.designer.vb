<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Switchboard
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Dim resources As System.ComponentModel.ComponentResourceManager = New System.ComponentModel.ComponentResourceManager(GetType(Switchboard))
        Me.cancelButton1 = New System.Windows.Forms.Button
        Me.exitButton = New System.Windows.Forms.Button
        Me.testButton = New System.Windows.Forms.Button
        Me.aboutButton = New System.Windows.Forms.Button
        Me.ActionsComboBox = New System.Windows.Forms.ComboBox
        Me.openButton = New System.Windows.Forms.Button
        Me.Panel1 = New System.Windows.Forms.Panel
        Me.Panel1.SuspendLayout()
        Me.SuspendLayout()
        '
        'cancelButton1
        '
        Me.cancelButton1.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.cancelButton1.DialogResult = System.Windows.Forms.DialogResult.Cancel
        Me.cancelButton1.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.cancelButton1.Location = New System.Drawing.Point(167, 38)
        Me.cancelButton1.Name = "cancelButton1"
        Me.cancelButton1.Size = New System.Drawing.Size(60, 23)
        Me.cancelButton1.TabIndex = 7
        Me.cancelButton1.Text = "&Cancel"
        '
        'exitButton
        '
        Me.exitButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.exitButton.DialogResult = System.Windows.Forms.DialogResult.OK
        Me.exitButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.exitButton.Location = New System.Drawing.Point(374, 38)
        Me.exitButton.Name = "exitButton"
        Me.exitButton.Size = New System.Drawing.Size(60, 23)
        Me.exitButton.TabIndex = 5
        Me.exitButton.Text = "E&xit"
        '
        'testButton
        '
        Me.testButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.testButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.testButton.Location = New System.Drawing.Point(236, 38)
        Me.testButton.Name = "testButton"
        Me.testButton.Size = New System.Drawing.Size(60, 23)
        Me.testButton.TabIndex = 8
        Me.testButton.Text = "&Test"
        '
        'aboutButton
        '
        Me.aboutButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Bottom Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.aboutButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.aboutButton.Location = New System.Drawing.Point(305, 38)
        Me.aboutButton.Name = "aboutButton"
        Me.aboutButton.Size = New System.Drawing.Size(60, 23)
        Me.aboutButton.TabIndex = 10
        Me.aboutButton.Text = "&About"
        '
        'ActionsComboBox
        '
        Me.ActionsComboBox.Anchor = CType(((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Left) _
                    Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.ActionsComboBox.BackColor = System.Drawing.SystemColors.Window
        Me.ActionsComboBox.Cursor = System.Windows.Forms.Cursors.Default
        Me.ActionsComboBox.Font = New System.Drawing.Font("Arial", 12.0!, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
        Me.ActionsComboBox.ForeColor = System.Drawing.SystemColors.WindowText
        Me.ActionsComboBox.Location = New System.Drawing.Point(9, 3)
        Me.ActionsComboBox.Name = "ActionsComboBox"
        Me.ActionsComboBox.RightToLeft = System.Windows.Forms.RightToLeft.No
        Me.ActionsComboBox.Size = New System.Drawing.Size(361, 27)
        Me.ActionsComboBox.TabIndex = 11
        Me.ActionsComboBox.Text = "Select option from the list"
        '
        'openButton
        '
        Me.openButton.Anchor = CType((System.Windows.Forms.AnchorStyles.Top Or System.Windows.Forms.AnchorStyles.Right), System.Windows.Forms.AnchorStyles)
        Me.openButton.Location = New System.Drawing.Point(376, 4)
        Me.openButton.Name = "openButton"
        Me.openButton.Size = New System.Drawing.Size(58, 24)
        Me.openButton.TabIndex = 12
        Me.openButton.Text = "&Open..."
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.ActionsComboBox)
        Me.Panel1.Controls.Add(Me.cancelButton1)
        Me.Panel1.Controls.Add(Me.exitButton)
        Me.Panel1.Controls.Add(Me.openButton)
        Me.Panel1.Controls.Add(Me.testButton)
        Me.Panel1.Controls.Add(Me.aboutButton)
        Me.Panel1.Dock = System.Windows.Forms.DockStyle.Bottom
        Me.Panel1.Location = New System.Drawing.Point(0, 199)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(442, 66)
        Me.Panel1.TabIndex = 15
        '
        'Switchboard
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(442, 265)
        Me.Controls.Add(Me.Panel1)
        Me.Icon = CType(resources.GetObject("$this.Icon"), System.Drawing.Icon)
        Me.Name = "Switchboard"
        Me.Text = "Form1"
        Me.Panel1.ResumeLayout(False)
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents cancelButton1 As System.Windows.Forms.Button
    Friend WithEvents exitButton As System.Windows.Forms.Button
    Friend WithEvents testButton As System.Windows.Forms.Button
    Friend WithEvents aboutButton As System.Windows.Forms.Button
    Public WithEvents ActionsComboBox As System.Windows.Forms.ComboBox
    Friend WithEvents openButton As System.Windows.Forms.Button
    Friend WithEvents Panel1 As System.Windows.Forms.Panel

End Class
