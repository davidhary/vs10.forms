<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class ExceptionDisplayTestPanel
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.localErrorButton = New System.Windows.Forms.Button
        Me.xsltButton = New System.Windows.Forms.Button
        Me.userButton = New System.Windows.Forms.Button
        Me.sqlButton = New System.Windows.Forms.Button
        Me.simpleButton = New System.Windows.Forms.Button
        Me.SuspendLayout()
        '
        'localErrorButton
        '
        Me.localErrorButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.localErrorButton.Location = New System.Drawing.Point(10, 136)
        Me.localErrorButton.Name = "localErrorButton"
        Me.localErrorButton.Size = New System.Drawing.Size(272, 29)
        Me.localErrorButton.TabIndex = 9
        Me.localErrorButton.Text = "Local Divide by zero"
        '
        'xsltButton
        '
        Me.xsltButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.xsltButton.Location = New System.Drawing.Point(10, 104)
        Me.xsltButton.Name = "xsltButton"
        Me.xsltButton.Size = New System.Drawing.Size(272, 29)
        Me.xsltButton.TabIndex = 8
        Me.xsltButton.Text = "XSLT Exception"
        '
        'userButton
        '
        Me.userButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.userButton.Location = New System.Drawing.Point(10, 72)
        Me.userButton.Name = "userButton"
        Me.userButton.Size = New System.Drawing.Size(272, 29)
        Me.userButton.TabIndex = 7
        Me.userButton.Text = "Complex User Exception"
        '
        'sqlButton
        '
        Me.sqlButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.sqlButton.Location = New System.Drawing.Point(10, 40)
        Me.sqlButton.Name = "sqlButton"
        Me.sqlButton.Size = New System.Drawing.Size(272, 29)
        Me.sqlButton.TabIndex = 6
        Me.sqlButton.Text = "SQL Exception"
        '
        'simpleButton
        '
        Me.simpleButton.FlatStyle = System.Windows.Forms.FlatStyle.System
        Me.simpleButton.Location = New System.Drawing.Point(10, 8)
        Me.simpleButton.Name = "simpleButton"
        Me.simpleButton.Size = New System.Drawing.Size(272, 29)
        Me.simpleButton.TabIndex = 5
        Me.simpleButton.Text = "Simple Exception"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(292, 174)
        Me.Controls.Add(Me.localErrorButton)
        Me.Controls.Add(Me.xsltButton)
        Me.Controls.Add(Me.userButton)
        Me.Controls.Add(Me.sqlButton)
        Me.Controls.Add(Me.simpleButton)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.ResumeLayout(False)

    End Sub
    Friend WithEvents localErrorButton As System.Windows.Forms.Button
    Private WithEvents xsltButton As System.Windows.Forms.Button
    Private WithEvents userButton As System.Windows.Forms.Button
    Private WithEvents sqlButton As System.Windows.Forms.Button
    Private WithEvents simpleButton As System.Windows.Forms.Button
End Class
