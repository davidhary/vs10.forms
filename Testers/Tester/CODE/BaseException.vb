Imports System.Runtime.Serialization
Imports System.Security.Permissions
Imports System.Reflection

''' <summary>
''' An inheritable exception for use by ISR framework classes and applications.
''' </summary>
''' <remarks>
''' Inherits from System.Exception per FxCop design rule CA1958 which specifies 
''' that "Types do not extend inheritance vulnerable types" and further explains 
''' that "This [Applicaiton Exception] base exception type does not provide any additional value for 
''' framework classes." 
''' </remarks>
<Serializable()> Public Class BaseException
    Inherits System.Exception

#Region " CONSTRUCTORS "

    ''' <summary>
    ''' A parameterless constructor.
    ''' </summary>
    Public Sub New()
        MyBase.New()
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>.
    ''' </summary>
    ''' <param name="message">
    ''' Specifies the exception message.
    ''' </param>
    Public Sub New(ByVal message As String)
        MyBase.New(message)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>
    ''' Constructs the class specifying a <paramref name="message">message</paramref>
    ''' and <paramref name="innerException"/>.
    ''' </summary>
    ''' <param name="message">
    ''' Specifies the exception message.
    ''' </param>
    ''' <param name="innerException">
    ''' Specifies the InnerException.
    ''' </param>
    Public Sub New(ByVal message As String, ByVal innerException As System.Exception)
        MyBase.New(message, innerException)
        obtainEnvironmentInformation()
    End Sub

    ''' <summary>
    ''' Constructs the class using serialization <paramref name="info"/> and <paramref name="context"/>
    '''  information.
    ''' </summary>
    ''' <param name="info">
    ''' Specifies <see cref="SerializationInfo">serialization information</see>.
    ''' </param>
    ''' <param name="context">
    ''' Sepecifies <see cref="StreamingContext">streaming context</see> for the exception.
    ''' </param>
    Protected Sub New(ByVal info As SerializationInfo, ByVal context As StreamingContext)
        MyBase.New(info, context)
        If info Is Nothing Then
            Return
        End If
        _additionalInformation = CType(info.GetValue("additionalInformation", _
            GetType(System.Collections.Specialized.NameValueCollection)), System.Collections.Specialized.NameValueCollection)
    End Sub

#End Region

#Region " METHODS AND PROPERTIES "

    ''' <summary>
    ''' Specifies the contents of the additional information.
    ''' </summary>
    ''' <remarks></remarks>
    Private Enum AdditionalInfoItem
        None
        MachineName
        Timestamp
        FullName
        AppDomainName
        ThreadIdentity
        WindowsIdentity
        OSVersion
    End Enum

    ''' <summary>
    ''' Overrides the <see cref="GetObjectData"/> method to serialize custom values.
    ''' </summary>
    ''' <param name="info">
    ''' Specifies <see cref="SerializationInfo">serialization information</see>.
    ''' </param>
    ''' <param name="context">
    ''' Sepecifies <see cref="StreamingContext">streaming context</see> for the exception.
    ''' </param>
    <SecurityPermission(SecurityAction.Demand, SerializationFormatter:=True), _
      SecurityPermission(SecurityAction.LinkDemand, Flags:=SecurityPermissionFlag.SerializationFormatter)> _
    Public Overrides Sub GetObjectData(ByVal info As SerializationInfo, ByVal context As StreamingContext)

        If info Is Nothing Then
            Return
        End If
        info.AddValue("AdditionalInformation", _additionalInformation, GetType(System.Collections.Specialized.NameValueCollection))
        MyBase.GetObjectData(info, context)

    End Sub

    ''' <summary>
    ''' Gathers environment information safely.
    ''' </summary>
    Private Sub obtainEnvironmentInformation()

        _additionalInformation = New System.Collections.Specialized.NameValueCollection
        _additionalInformation.Add(AdditionalInfoItem.MachineName.ToString, My.Computer.Name)
        _additionalInformation.Add(AdditionalInfoItem.Timestamp.ToString, Date.Now.ToString(System.Globalization.CultureInfo.CurrentCulture))
        _additionalInformation.Add(AdditionalInfoItem.FullName.ToString, Assembly.GetExecutingAssembly().FullName)
        _additionalInformation.Add(AdditionalInfoItem.AppDomainName.ToString, AppDomain.CurrentDomain.FriendlyName)
        _additionalInformation.Add(AdditionalInfoItem.ThreadIdentity.ToString, My.User.Name) ' Thread.CurrentPrincipal.Identity.Name
        _additionalInformation.Add(AdditionalInfoItem.WindowsIdentity.ToString, My.Computer.Info.OSFullName)
        _additionalInformation.Add(AdditionalInfoItem.OSVersion.ToString, My.Computer.Info.OSVersion)

    End Sub

    Private _additionalInformation As New System.Collections.Specialized.NameValueCollection
    ''' <summary>
    ''' Collection allowing additional information to be added to the exception.
    ''' </summary>
    Public ReadOnly Property AdditionalInformation() As System.Collections.Specialized.NameValueCollection
        Get
            Return _additionalInformation
        End Get
    End Property

#End Region

End Class

